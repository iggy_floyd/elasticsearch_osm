FROM rastasheep/ubuntu-sshd:14.04
MAINTAINER Igor Marfin <iggy.floyd.de@gmail.com>

RUN apt-get update

# Install basic software
RUN apt-get -y install wget

# Change locale to en_US.UTF-8
RUN locale-gen en_US.UTF-8  
RUN dpkg-reconfigure locales
RUN update-locale LANG=en_US.UTF-8 LANGUAGE=en_US:en LC_ALL=en_US.UTF-8 
ENV LANG en_US.UTF-8  
ENV LANGUAGE en_US:en  
ENV LC_ALL en_US.UTF-8 


# Note: libgeos++-dev is included here too (the nominatim install page suggests installing it if there is a problem with the 'pear install DB' below - it seems safe to install it anyway)
RUN apt-get update && apt-get -y install build-essential
RUN apt-get update && apt-get -y install gcc git osmosis
RUN apt-get update && apt-get -y install libxml2-dev libgeos-dev libpq-dev libbz2-dev libtool automake libproj-dev \
 proj-bin libgeos-c1 libgeos++-dev libexpat1-dev

# Install Boost (required by osm2pqsql)
RUN apt-get update && apt-get -y install autoconf make g++ libboost-dev \
  libboost-system-dev libboost-filesystem-dev libboost-thread-dev

# Install PHP5
RUN apt-get update && apt-get -y install php5 php-pear php5-pgsql php5-json php-db

# From the website "If you plan to install the source from github, the following additional packages are needed:"
# RUN apt-get -y install git autoconf-archive

# Install Postgres, PostGIS and dependencies
RUN apt-get -y install postgresql postgis postgresql-contrib postgresql-9.3-postgis-2.1 postgresql-server-dev-9.3


# Some additional packages that may not already be installed
# bc is needed in configPostgresql.sh
RUN apt-get -y install bc


# Install Apache
RUN apt-get -y install apache2

# Add Protobuf support
RUN apt-get -y install libprotobuf-c0-dev protobuf-c-compiler

RUN apt-get install -y sudo

RUN apt-get install -y  lua5.2  liblua5.2-dev  liblua5.2-0

#
RUN pear install DB
RUN useradd -m -p password1234 nominatim
RUN mkdir -p /app/nominatim
RUN chown nominatim: /app/nominatim
RUN git clone --recursive https://github.com/twain47/Nominatim.git /app/nominatim
RUN cd /app/nominatim
WORKDIR /app/nominatim
RUN ./autogen.sh
RUN ./configure
RUN make


# Add VOLUMEs to allow backup of config, logs and databases!
# Also this solves the problem of "FATAL:  the database system is starting up"
RUN mkdir -p /var/run/postgresql && chown -R postgres:postgres /var/run/postgresql
RUN mkdir -p /var/lib/postgresql && chown -R postgres:postgres /var/lib/postgresql
RUN mkdir -p /var/log/postgresql && chown -R postgres:postgres /var/log/postgresql
RUN mkdir -p /etc/postgresql && chown -R postgres:postgres /etc/postgresql
VOLUME ["/etc/postgresql", "/var/log/postgresql", "/var/lib/postgresql"]  


WORKDIR /app/nominatim

ADD ./provision/local.php /app/nominatim/settings/local.php
ADD ./provision/setup.php /app/nominatim/utils/setup.php
RUN chmod +x /app/nominatim/utils/setup.php


RUN mkdir -p /var/www/nominatim
RUN ls settings/
RUN cat settings/local.php
RUN ./utils/setup.php --create-website /var/www/nominatim

# Write out a robots file to keep search engines out
ADD ./provision/forbid_robots.sh /app/nominatim/forbid_robots.sh
RUN chmod +x /app/nominatim/forbid_robots.sh 
RUN /app/nominatim/forbid_robots.sh 


# Adjust PostgreSQL configuration so that remote connections to the


# Expose the PostgreSQL port
EXPOSE 5432

RUN apt-get install -y curl
ADD ./provision/400-nominatim.conf /etc/apache2/sites-available/400-nominatim.conf
ADD ./provision/httpd.conf /etc/apache2/httpd.conf
RUN service apache2 start && \
  a2ensite 400-nominatim.conf && \
  /etc/init.d/apache2 reload

# Expose the HTTP port
EXPOSE 8080


ADD ./provision/configPostgresql.sh /app/nominatim/configPostgresql.sh
ADD ./provision/configPostgresqlDiskWrites.sh /app/nominatim/configPostgresqlDiskWrites.sh
ADD ./provision/start.sh /app/nominatim/start.sh
ADD ./provision/prepare_db.sh /app/nominatim/prepare_db.sh
ADD ./provision/deploy.sh /app/nominatim/deploy.sh
RUN chmod +x /app/nominatim/configPostgresql.sh
RUN chmod +x /app/nominatim/configPostgresqlDiskWrites.sh
RUN chmod +x /app/nominatim/start.sh
RUN chmod +x /app/nominatim/prepare_db.sh
RUN chmod +x /app/nominatim/deploy.sh



# Run ME
# mkdir etc_postgresql 
# mkdir log_postgresql
# mkdir lib_postgresql
# sudo docker build -t nominatim_engine .
# JOB_DOCKER=$(sudo docker run -d -p 2222:22 -p 8080:8080 `pwd`/etc_postgresql:/etc/postgresql -v `pwd`/log_postgresql:/var/log/postgresql -v `pwd`/lib_postgresql:/var/lib/postgresql  --name=nominatim_engine nominatim_engine /app/nominatim/prepare_db.sh europe/monaco-latest.osm.pbf)
# HOST_DOCKER=$(sudo docker inspect $JOB_DOCKER | grep IPAddress | cut -d '"' -f 4 | tail -n 1)
# use a password: 'root'
# ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@$HOST_DOCKER

# or
# sudo docker run -t --rm -i -p 8080:8080 -v `pwd`/etc_postgresql:/etc/postgresql -v `pwd`/log_postgresql:/var/log/postgresql -v `pwd`/lib_postgresql:/var/lib/postgresql --name=nominatim_engine nominatim_engine /app/nominatim/prepare_db.sh europe/monaco-latest.osm.pbf
#
# or
# sudo docker run -t --rm -i -p 8080:8080 -v `pwd`/etc_postgresql:/etc/postgresql -v `pwd`/log_postgresql:/var/log/postgresql -v `pwd`/lib_postgresql:/var/lib/postgresql --name=nominatim_engine nominatim_engine /app/nominatim/start.sh

# test the nominatim server
# curl localhost:8080

