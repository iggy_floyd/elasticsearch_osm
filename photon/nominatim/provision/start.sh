#!/bin/bash
service postgresql start
/etc/init.d/ssh start
#if [ -z $OPTIMIZE_POSTGRESQL ]; then
#  ./configPostgresql.sh dw n
#  service postgresql restart

# ./configPostgresqlDiskWrites.sh  
# service postgresql restart
#fi

# start the apache2 in FOREGROUND
tail -f /var/log/apache2/* &
/usr/sbin/apache2ctl -D FOREGROUND



# Updating Nominatim
## PLEASE, START the apache2 as a SERVICE!
# Using two threads for the upadate will help performance, by adding this option: --index-instances 2
# Going much beyond two threads is not really worth it because the threads interfere with each other quite a bit.
 #  If your system is live and serving queries, keep an eye on response times at busy times, because too many update threads might interfere there, too.
#sudo -u nominatim ./utils/update.php --import-osmosis-all --no-npi
