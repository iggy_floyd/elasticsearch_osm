#!/bin/bash

[[ "${_docker_deployment_nominatim}" == "import" ]] &&  /app/nominatim/prepare_db.sh "$1"
/app/nominatim/start.sh

