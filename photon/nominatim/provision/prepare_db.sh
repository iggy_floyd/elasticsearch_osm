#!/bin/bash
wget --output-document=/app/data.pbf http://download.geofabrik.de/"${1}"

./utils/setup.php --help

chown -R postgres:postgres /var/run/postgresql
chown -R postgres:postgres /var/lib/postgresql
chown -R postgres:postgres /etc/postgresql/9.3

service postgresql start && \
  pg_dropcluster --stop 9.3 main || echo "Some problems encountered!"
service postgresql start && \
  pg_createcluster --start -e UTF-8 9.3 main || echo "Some problems encountered!"


service postgresql start && \
  sudo -u postgres psql postgres -tAc "SELECT 1 FROM pg_roles WHERE rolname='nominatim'" | grep -q 1 || sudo -u postgres createuser -s nominatim && \
  sudo -u postgres psql postgres -tAc "SELECT 1 FROM pg_roles WHERE rolname='www-data'" | grep -q 1 || sudo -u postgres createuser -SDR www-data && \
  sudo -u postgres psql postgres -c "DROP DATABASE IF EXISTS nominatim" && \
  sudo -u nominatim ./utils/setup.php --osm-file /app/data.pbf --all --threads 2 && \
  sudo -u nominatim ./utils/setup.php --create-functions --enable-diff-updates

echo "host all  all    0.0.0.0/0  trust" >> /etc/postgresql/9.3/main/pg_hba.conf
echo "listen_addresses='*'" >> /etc/postgresql/9.3/main/postgresql.conf

#if [ -z $OPTIMIZE_POSTGRESQL ]; then
  ./configPostgresql.sh dw n
  service postgresql restart

 ./configPostgresqlDiskWrites.sh  
 service postgresql restart
#fi

# start the apache2 in FOREGROUND
tail -f /var/log/apache2/* &
/usr/sbin/apache2ctl -D FOREGROUND




# Updating Nominatim
## PLEASE, START the apache2 as a SERVICE!
# Using two threads for the upadate will help performance, by adding this option: --index-instances 2
# Going much beyond two threads is not really worth it because the threads interfere with each other quite a bit.
 #  If your system is live and serving queries, keep an eye on response times at busy times, because too many update threads might interfere there, too.
#sudo -u nominatim ./utils/update.php --import-osmosis-all --no-npi
