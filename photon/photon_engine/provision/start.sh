#!/bin/bash
/etc/init.d/ssh start
[[ "$1" == "import_from_nominatim" ]] &&  java -jar /photon/photon-0.2.5.jar -nominatim-import -host $NOMINATIM_PORT_5432_TCP_ADDR -port $NOMINATIM_PORT_5432_TCP_PORT -database nominatim -user nominatim &  java -jar /photon/photon-0.2.5.jar
[[ "$1" == "update_from_nominatim" ]] &&  java -jar /photon/photon-0.2.5.jar  -host $NOMINATIM_PORT_5432_TCP_ADDR -port $NOMINATIM_PORT_5432_TCP_PORT -database nominatim -user nominatim -password nominatim
[[ "$1" == "use_stored_db" ]] &&  java -jar /photon/photon-0.2.5.jar 
