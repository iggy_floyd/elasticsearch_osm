#!/bin/bash


[[ "${_docker_deployment_photon}" == "import" ]] &&  /photon/start.sh import_from_nominatim
[[ "${_docker_deployment_photon}" == "normal" ]] &&  /photon/start.sh use_stored_db

/photon/start.sh update_from_nominatim

