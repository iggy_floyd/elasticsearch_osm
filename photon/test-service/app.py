import os


DEBUG = os.environ.get('DEBUG', False)
PORT = os.environ.get('PHOTON_PORT', 5002)
HOST = os.environ.get('PHOTON_HOST', '0.0.0.0')
API_URL = os.environ.get('API_URL', 'http://localhost:5001/api/?')
DEPLOYMENTCMD=os.environ.get('DEPLOYMENTCMD','')
print "app.py:", HOST
print "app.py:", PORT
print "app.py:", API_URL
print "app.py:", DEPLOYMENTCMD



if __name__ == "__main__":
   print "Hello world!"
