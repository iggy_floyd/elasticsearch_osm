#!/bin/bash

trap_function(){
  echo "stopping now!"
  sudo /etc/init.d/apache2 stop
  sudo /etc/init.d/mysql stop
  kill -TERM $PID
  wait $PID
  echo "sleeping..."
  sleep 2
}




export PHOTON_PORT_2322_TCP_ADDR=$PHOTON_PORT_2322_TCP_ADDR
export PHOTON_PORT_2322_TCP_PORT=$PHOTON_PORT_2322_TCP_PORT
main() {
eval $'cat <<\002\n'"$(</home/vagrant/project/provision/.htaccess)"$'\n\002' > /home/vagrant/project/www/address.validator.com/public/.htaccess

sudo /etc/init.d/apache2 restart
sudo /etc/init.d/mysql restart
# it allows to ssh to the container
/usr/sbin/sshd -D &
PID=$!

echo "ElasticOSM Host: $PHOTON_PORT_2322_TCP_ADDR"
echo "ElasticOSM Port: $PHOTON_PORT_2322_TCP_PORT"
}

trap 'trap_function' TERM 
main
echo "PID=$PID"
wait $PID

