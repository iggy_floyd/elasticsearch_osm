<?php

class Address_Model extends TinyMVC_Model
{
    /**
     * @return array
     */
    public function getAddress()
    {
        $results = array();
        $this->db->query('select * from address');
        while($row = $this->db->next())
            $results[] = $row;
        return $results;
    }

    public function getLimitedAddress($offset = 0,$limit = 10)
    {
        $results = array();
        $this->db->select('*'); // set selected columns
        $this->db->from('address');
        $this->db->orderby('id DESC');
        $this->db->limit($limit,$offset);
        $this->db->query();
        while($row = $this->db->next())
            $results[] = $row;
        return $results;
    }

    public function findEmptyProviders($limit = 10) {
        $results = array();
        $this->db->select('*'); // set selected columns
        $this->db->from('address');
        $this->db->where('last_provider','');
        $this->db->orderby('id DESC');
        $this->db->limit($limit);
        $this->db->query();
        while($row = $this->db->next())
            $results[] = $row;
        return $results;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function insertAddress($data)
    {
        return $this->db->insert('address', $data);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function updateAddress($data)
    {
        $this->db->where('id',$data['id']);
        unset($data['id']);
        return $this->db->update('address', $data);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getAddressinfoByRequestId($id)
    {
        $results = array();
        $this->db->query("select count(*) as cnt from address where request_id =" . $id);
        $row = $this->db->next();
        $results['count'] = $row['cnt'];
        $this->db->query("select count(*) as cnt from address where request_id =" . $id . " and status = 1");
        $row = $this->db->next();
        $results['status'] = $row['cnt'];
        $this->db->query("select count(*) as cnt from address where request_id =" . $id . " and error = 1");
        $row = $this->db->next();
        $results['error'] = $row['cnt'];

        $results['id'] = $id;

        return $results;
    }
}