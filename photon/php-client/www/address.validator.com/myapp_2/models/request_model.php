<?php

class Request_Model extends TinyMVC_Model
{
    /**
     * Create request and save to DB
     *
     * @param $user
     * @return mixed
     */
    public function createRequest($user)
    {
        $data['user_id'] = $user;
        return $this->db->insert('request', $data);
    }

    /**
     * Get requests with offset und limit
     *
     * @param $offset
     * @param $limit
     * @return array
     */
    public function getRequests($offset = 0,$limit = 10)
    {
        $results = array();
        $this->db->select('*'); // set selected columns
        $this->db->from('request');
        $this->db->orderby('id DESC');
        $this->db->limit($limit,$offset);
        $this->db->query();
        while($row = $this->db->next())
            $results[] = $row;
        return $results;
    }

    public function updateRequest($data)
    {
        $this->db->where('id',$data['id']);
        unset($data['id']);
        return $this->db->update('request', $data);
    }
}