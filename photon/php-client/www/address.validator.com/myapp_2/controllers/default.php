<?php

/**
 * default.php
 *
 * default application controller
 *
 * @package		TinyMVC
 * @author		Monte Ohrt
 */

class Default_Controller extends Base_Controller
{
  function index()
  {
      // load the models:

      // the address model
      $this->load->model('Address_Model', 'address');
      $addresses=$this->address->getLimitedAddress();
      // test of the model
      //$addresses=$this->address->findEmptyProviders(200);
      //var_dump($addresses[1]['city']);
      $this->tpl->assign('addresses', $addresses);

      // the request model
      $this->load->model('Request_Model','request');
      $requests = $this->request->getRequests();
      $this->tpl->assign('requests', $requests);


  }

  private function _build_table($array){
    // start table
    $html = '<table class="table table-bordered table-hover table-striped">';
    // header row
    $html .= '<tr>';
    foreach($array as $key=>$value){
        $html .= '<th>' . $key . '</th>';
    }
    $html .= '</tr>';

    $html .= '<tr>';
    foreach( $array as $key=>$value){
        $html .= '<td>' . $value . '</td>';
    }
    $html .= '</tr>';
    // finish table and return it
    $html .= '</table>';
    return $html;
  }

  function mail(){

      if ($this->is_post()) {

          $data = $this->post();
          if (empty($data['name']) ||
              empty($data['email']) 		||
              empty($data['phone']) 		||
              empty($data['message'])	||
              !filter_var($data['email'],FILTER_VALIDATE_EMAIL)
          ) {
              echo "No arguments Provided!";
              return false;
          }

        $name = $data['name'];
        $email_address = $data['email'];
        $phone = $data['phone'];
        $message = $data['message'];

// Create the email and send the message
        $to = 'iggy.floy.de@gmail.com'; // Add your email address inbetween the '' replacing yourname@yourdomain.com - This is where the form will send a message to.
        $email_subject = "Website Contact Form:  $name";
        $email_body = "You have received a new message from your website contact form.\n\n"."Here are the details:\n\nName: $name\n\nEmail: $email_address\n\nPhone: $phone\n\nMessage:\n$message";
        $headers = "From: noreply@yourdomain.com\n"; // This is the email address the generated message will be from. We recommend using something like noreply@yourdomain.com.
        $headers .= "Reply-To: $email_address";
        mail($to,$email_subject,$email_body,$headers);
        return true;
      }
      return false;
  }

  function mydaemon(){

      $status = false;
      $response='Daemon undefined';
      if ($this->is_post()) {
          $data = $this->post();

          $this->load->model('SystemDaemon_Model', 'systemdaemon', null, 'systemdaemon');
          $_mydaemon=$this->systemdaemon->getDaemon();
          $_mydaemon->parent = $this;
          $_mydaemon->run = function($options=null) {
              // This variable keeps track of how many 'runs' or 'loops' your daemon has
              // done so far. For example purposes, we're quitting on 3.
              $cnt = 1;

              // This variable gives your own code the ability to breakdown the daemon:
              $runningOkay = true;

              //while (!System_Daemon::isDying() && $runningOkay && $cnt <=5) {
              while (!System_Daemon::isDying() && $runningOkay ) {

                  //this peace of the code is relevant only when the daemon is not spawned
                  if (!is_null($options)) {
                      if (getLock($options['appPidLocation'],false)){
                          System_Daemon::log(System_Daemon::LOG_INFO,"Lock was released: Stopping Execution!");
                          break;
                      }
                  }

                  // What mode are we in?
                  $mode = '"'.(System_Daemon::isInBackground() ? '' : 'non-' ).
                      'daemon" mode';

                  System_Daemon::log(System_Daemon::LOG_INFO, "Daemon: '".
                      System_Daemon::getOption("appName").
                      "' spawned! This will be written to ".
                      System_Daemon::getOption("logLocation"));

                  System_Daemon::info('{appName} running in %s %s/3',
                      $mode,
                      $cnt
                  );


                  //In the actuall logparser program, You could replace 'true'
                  // With e.g. a  parseLog('vsftpd') function, and have it return
                  // either true on success, or false on failure.
                  extract(array('this_daemon' => requesting_class('daemon'))); // Determine what class is responsible for making the call to Closure
                  //var_dump($this_daemon);
                  //extract(array('this_daemon' => requesting_class())); // Determine what class is responsible for making the call to Closure
                  //System_Daemon::log(System_Daemon::LOG_INFO,serialize($this_daemon));
                  //System_Daemon::log(System_Daemon::LOG_INFO,$this_daemon);
                  System_Daemon::log(System_Daemon::LOG_INFO,"1 Daemon status ? -->".stdObject::$status);
                  System_Daemon::log(System_Daemon::LOG_INFO,"2 Daemon has stop ? -->".isset($this_daemon->stopflag));
                  System_Daemon::log(System_Daemon::LOG_INFO,"3 Daemon has stop ? -->".$this_daemon->stopflag);
                
                  //System_Daemon::log(System_Daemon::LOG_INFO,"4 Daemon has parent ? -->".isset($this_daemon->parent));
                  //$runningOkay = isset($this_daemon->stopflag) ||  isset($this->stopflag) ? false:true;

                  $runningOkay = stdObject::$status;




                  // actually we want to read database and check addresses
                  // DOESN'T WORK!!!
                  System_Daemon::log(System_Daemon::LOG_INFO,"0000");
                  $this_daemon->load->model('Address_Model', 'address');
                  System_Daemon::log(System_Daemon::LOG_INFO,"1111");
                  $addresses=$this_daemon->address->getLimitedAddress();
                  System_Daemon::log(System_Daemon::LOG_INFO,"2222");
                  System_Daemon::log(System_Daemon::LOG_INFO,  gettype($addresses));
                  System_Daemon::log(System_Daemon::LOG_INFO,  count($addresses));
                  
                  //System_Daemon::log(System_Daemon::LOG_INFO,'Number of found addresses '.string(count($addresses)));
                  //System_Daemon::log(System_Daemon::LOG_INFO,"First city %s",$addresses[0]['city']);



                  // Level 4 would be fatal and shuts down the daemon immediately,
                  // which in this case is handled by the while condition.
                  if (!$runningOkay) {
                      System_Daemon::err('daemon produced an error, '.
                          'so this will be my last run');
                  }

                  // Relax the system by sleeping for a little bit
                  // iterate also clears statcache
                  System_Daemon::iterate(15);

                  $cnt++;
              }

          };


          $start_daemon = $data['start'];

          //var_dump($daemon);
          $_mydaemon->spawned=false;
          if ($start_daemon === 'true') {
              $this->stopflag=false;
              stdObject::$status=true;
              $_mydaemon->start();
              $response='Daemon started';
          } else {
              $_mydaemon->stop();
              $response='Daemon stoped';
          }
          $status=true;
          //$response = $data;
          //var_dump($data);
      }
      return  json_encode(array('status' => $status, 'response' => $response));
  }

  public function addfile() {
        $status = false;
        $response = 'Undefined';
        if ($this->is_post()) {

            // load the models
            $this->load->model('Address_Model', 'address');
            $this->load->model('Request_Model', 'request');
            $target_file = $_FILES['user_file']['tmp_name'];
            $csvFile = file($target_file);
            $requestId = $this->request->createRequest(1);
            $tmp = array();
            foreach ($csvFile as $rowCSV) {
                $csvArray = explode(';', trim($rowCSV));
                //'DE';'Street_Name';'House_Nr';'ZIP';'City'
                //only correctly rows in db save
                if (count($csvArray) == 5) {
                    $data['country'] = $csvArray[0];
                    $data['street'] = $csvArray[1];
                    $data['house_nr'] = $csvArray[2];
                    $data['zip'] = $csvArray[3];
                    $data['city'] = $csvArray[4];
                    $data['request_id'] = $requestId;
                    $addressId = $this->address->insertAddress($data);
                }
            }
            $status = true;
            $response = 'Uploaded';


            // add a daemon
            $this->load->model('SystemDaemon_Model', 'systemdaemon', null, 'systemdaemon');
            $_mydaemon=$this->systemdaemon->getDaemon();
            $_mydaemon->run = function($options=null) {
                // This variable keeps track of how many 'runs' or 'loops' your daemon has
                $cnt = 1;
                // This variable gives your own code the ability to breakdown the daemon:
                $runningOkay = true;
                while (!System_Daemon::isDying() && $runningOkay ) {
                    //this peace of the code is relevant only when the daemon is not spawned
                    if (!is_null($options)) {
                        if (getLock($options['appPidLocation'],false)){
                            System_Daemon::log(System_Daemon::LOG_INFO,"Lock was released: Stopping Execution!");
                            break;
                        }
                    }
                    // What mode are we in?
                    $mode = '"'.(System_Daemon::isInBackground() ? '' : 'non-' ).
                        'daemon" mode';
                    System_Daemon::log(System_Daemon::LOG_INFO, "Daemon: '".
                        System_Daemon::getOption("appName").
                        "' spawned! This will be written to ".
                        System_Daemon::getOption("logLocation"));
                    System_Daemon::info('{appName} running in %s %s',
                        $mode,
                        $cnt
                    );

                    extract(array('this_daemon' => requesting_class('daemon'))); // Determine what class is responsible for making the call to Closure
                    System_Daemon::log(System_Daemon::LOG_INFO,"Uploading Models...");
                    $this_daemon->load->model('Address_Model', 'address');
                    $this->load->model('AddressValidator_Model', 'addressvalidator', null, 'addressvalidator');
                    System_Daemon::log(System_Daemon::LOG_INFO,"Uploading Models...done");
                    System_Daemon::log(System_Daemon::LOG_INFO,"Search for pending addresses...");
                    $addresses=$this->address->findEmptyProviders(1000);
                    try {
                        foreach($addresses as $address) {
                            $response_service = $this->addressvalidator->getClient()->checkAddress(
                                $address['street'],
                                $address['house_nr'],
                                $address['zip'],
                                $address['city'],
                                $address['country'],
                                " "
                            )->filter("osm_key",array('building','place','highway'));
                            $coordinates = $response_service->coordinates();
                            $validity = count($coordinates)>0 ? 0:1; // validity
                            $streets = $response_service->streets();
                            $housenumbers = $response_service->housenumbers();
                            $postcodes = $response_service->postcodes();
                            $cities = $response_service->cities();
                            $response = array(
                                'status' => $validity,
                                'latitude' => ($validity == 0)?$coordinates[0][0]:-1.0,
                                'longitude' => ($validity == 0)?$coordinates[0][1]:-1.0,
                                'last_provider' => 'ElasticOSM',
                                'correct_address' =>  ($validity == 0)? implode(', ',array($streets[0],isset($housenumbers[0])? $housenumbers[0]:"",$postcodes[0],$cities[0])) : "",
                                'error' => 0,
                            );
                            $response['id'] = $address['id'];
                            $this->address->updateAddress($response);
                            $data = array();
                            $data['id'] = $address['request_id'];
                            $data['status'] = 0;
                            $data['count'] = 1;
                            $this->request->updateRequest($data);
                        }
                    } catch (AddressValidator_Client_Exception_HttpException $e) {
                        //Do nothing
                    }
                    System_Daemon::log(System_Daemon::LOG_INFO,"Pending addresses...processed");
                    if (!$runningOkay) {
                        System_Daemon::err('daemon produced an error, '.
                            'so this will be my last run');
                    }
                    // Relax the system by sleeping for a little bit
                    // iterate also clears statcache
                    System_Daemon::iterate(2);
                    $cnt++;
                }
            };
            // start the daemon if it's not started
            $_mydaemon->spawned=false;
            $_mydaemon->start();


        }
      return  json_encode(array('status' => $status, 'response' => $response));
  }
  function  add() {

/*
      $status = true;
      return json_encode(array('status' => $status, 'response' =>'Privet'));
*/

      $status = false;
      $response=array();
      $output='<h2>No Address Found</h2><br>';

      if ($this->is_post()) {

          // load the models
          $this->load->model('Address_Model', 'address');
          $this->load->model('Request_Model', 'request');
          $requestId = $this->request->createRequest(1);
          $data = $this->post();
          $data['request_id'] = $requestId;
          $addressId = $this->address->insertAddress($data);

          //request to service
          $this->load->model('AddressValidator_Model', 'addressvalidator', null, 'addressvalidator');
          try {
              $response_service = $this->addressvalidator->getClient()->checkAddress(
                  $data['street'],
                  $data['house_nr'],
                  $data['zip'],
                  $data['city'],
                  'DE',
                  " "
              )->filter("osm_key",array('building','place','highway'));

              $coordinates = $response_service->coordinates();
              $validity = count($coordinates)>0 ? 0:1; // validity
              $streets = $response_service->streets();
              $housenumbers = $response_service->housenumbers();
              $postcodes = $response_service->postcodes();
              $cities = $response_service->cities();



              $response = array(
                  'status' => $validity,
                  'latitude' => ($validity == 0)?$coordinates[0][0]:-1.0,
                  'longitude' => ($validity == 0)?$coordinates[0][1]:-1.0,
                  'last_provider' => 'ElasticOSM',
                  'correct_address' =>  "",
                  'error' => 0,
              );

              $response['id'] = $addressId;
              $this->address->updateAddress($response);

              $data = array();
              $data['id'] = $requestId;
              $data['status'] = 1;
              $data['count'] = 1;
              $this->request->updateRequest($data);

              for ($i = 0; $i < count($streets); ++$i) {
/*
                  echo "i=$i";
                  echo "stre=$streets[$i]";
                  $a = isset($housenumbers[$i])? $housenumbers[$i]:"";
                  echo "hou=$a";
                  echo "post=$postcodes[$i]";
                  echo "city=$cities[$i]";

*/                  $response = array(
                      'status' => $validity,
                      'latitude' => ($validity == 0)?$coordinates[$i][0]:-1.0,
                      'longitude' => ($validity == 0)?$coordinates[$i][1]:-1.0,
                      'last_provider' => 'ElasticOSM',
                      'correct_address' => ($validity == 0)? implode(', ',array($streets[$i],isset($housenumbers[$i])? $housenumbers[$i]:"",$postcodes[$i],$cities[$i])) : "",
                      'error' => 0,
                  );
                  $response['id'] = $addressId;
                  $this->address->updateAddress($response);


                  $data = array();
                  $data['id'] = $requestId;
                  $data['status'] = $response['status'];
                  $data['count'] = 1;
                  $this->request->updateRequest($data);

                  if ($i == 0) {
                      $output='<br>';
                  }
                  $output=$output.  $this->_build_table($response).'<br>';
              }
//              var_dump($output);
/*
              $response = array(
                  'status' => $validity,
                  'latitude' => ($validity == 0)?$coordinates[0][0]:-1.0,
                  'longitude' => ($validity == 0)?$coordinates[0][1]:-1.0,
                  'last_provider' => 'ElasticOSM',
                  'correct_address' => ($validity == 0)? implode(', ',array($streets[0],$housenumbers[0],$postcodes[0],$cities[0])) : "",
                  'error' => 0,
              );

              //Address update with the obtained response
              $response['id'] = $addressId;
              $this->address->updateAddress($response);


              //Request status update
              $data = array();
              $data['id'] = $requestId;
              $data['status'] = $response['status'];
              $data['count'] = 1;
              $this->request->updateRequest($data);
*/

              // status is OK
              $status = true;

// An example of the exception thrown!
//            throw new AddressValidator_Client_Exception_HttpException(new Exception("ERROR!!!!",512));
          } catch (AddressValidator_Client_Exception_HttpException $e) {
//              AddressValidator_Debugger::log($e->getMessage());
//              $this->tpl->assign('addressvalidatoroutput', $e->getMessage());
              return json_encode(array('status' => true, 'response' => implode(',', array($e->getMessage(),$e->getCode()))));
          }


      }

   //   return  json_encode(array('status' => $status, 'response' => implode(',', $response)));
   //   return  json_encode(array('status' => $status, 'response' => implode(',', $this->_build_table($response))));
      return  json_encode(array('status' => $status, 'response' => $output));
//      return  json_encode(array('status' => $status, 'response' =>  $this->_build_table($response)));
  }

}

?>
