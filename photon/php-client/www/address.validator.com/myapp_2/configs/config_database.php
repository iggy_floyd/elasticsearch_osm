<?php

/**
 * database.php
 *
 * application database configuration
 *
 * @package		TinyMVC
 */

$config['default']['plugin'] = 'TinyMVC_PDO'; // plugin for db access
$config['default']['type'] = 'mysql';      // connection type
$config['default']['host'] = 'localhost';  // db hostname
$config['default']['name'] = 'address';     // db name
$config['default']['user'] = 'root';     // db username
$config['default']['pass'] = 'dev';     // db password
$config['default']['persistent'] = false;  // db connection persistence?

// AddressValidator settings
$config['addressvalidator']['plugin'] = 'TinyMVC_AddressValidator'; // plugin to communicate with FPS ML detector
$config['addressvalidator']['host'] = isset($_SERVER['PHOTON_HOST']) ?  $_SERVER['PHOTON_HOST']:  (isset($_ENV['PHOTON_HOST']) ? $_ENV['PHOTON_HOST']:getenv('PHOTON_PORT_2322_TCP_ADDR')); // ElasticOSM  Host
$config['addressvalidator']['port'] = isset($_SERVER['PHOTON_PORT']) ?  $_SERVER['PHOTON_PORT']:  (isset($_ENV['PHOTON_PORT']) ? $_ENV['PHOTON_PORT']:getenv('PHOTON_PORT_2322_TCP_PORT'));  // ElasticOSM  Port
$config['addressvalidator']['debug'] = true;  // the debugging mode is ON

$config['systemdaemon']['plugin'] = 'TinyMVC_SystemDaemon'; // plugin to start daemons
$config['systemdaemon']['debug'] = true;  // the debugging mode is ON
