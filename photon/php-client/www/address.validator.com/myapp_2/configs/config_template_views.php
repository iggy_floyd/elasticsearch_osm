<?php

/**
 *
 *
 * configuration for view templates
 *
 * @package		TinyMVC
 */

// /test
$config['test']['title'] = 'This is a test page'; // title of the page
$config['test']['text'] = 'Hello world!';

// common settings for all routes
$config['common']['title'] = 'Address Validator'; // title of the page
$config['common']['navbar']=array(
    'Check'=>array(
        'name' => 'Validate Addresses',
        'link' => '#check',
        'template' =>'check_view.twig'
    ),
    'Results'=>array(
        'name' => 'Results of Validation',
        'link' => '#tables',
        'template' => 'tables_view.twig',
    ),
/*
    'Portfolio'=>array(
       'name' => 'Portfolio',
        'link' => '#portfolio',
        'template' =>'portfolio_view.twig'
    ),
*/
    'About' =>array(
        'name' => 'about',
        'link' => '#about',
        'template' =>'about_view.twig'
    ),
    'Contact' =>array(
        'name' => 'Contact',
        'link' => '#contact',
        'template' =>'contact_view.twig'
    ),
    'Daemon'=>array(
        'name' =>'Daemon',
        'link'=>'#daemon',
        'template' =>'daemon_view.twig'
    ),

    'DatabaseAdmin'=>array(
        'name' => 'Database Admin',
        'link' => '/adminer-4.2.3.php'
)
);

$config['common']['header']=array(
    'title'=>'Address Validation with OSM',
    'subtitle'=>'An experiment with Elastic-OSM framework',
    'template'=>'header_view.twig'
);

$config['common']['footer']=array(
    'title'=>'About Address Validator',
    'text'=>"<p>Address Validator is a tool created by Igor Marfin.</p>",
    'template' =>'footer_view.twig'
);



// / (default)
$config['default']['title']=$config['common']['title'];
$config['default']['navbar'] =  $config['common']['navbar'];
$config['default']['header'] =  $config['common']['header'];
$config['default']['footer'] =  $config['common']['footer'];

