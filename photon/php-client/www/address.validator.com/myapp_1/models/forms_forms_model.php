<?php

/* 
 *  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */


// some trick: Now a model can load  any plugin :-)
class Forms_Forms_Model extends TinyMVC_Controller
{

    public $forms=array(
        'single_validation_form',
        'batch_validation_form',
    );
    
    function get_forms() {

        // Now this model can generate forms
        $this->load->library('JForm_Wrapper','jform');
        
        // 'single_validation_form'
        $this->jform->setForm(
                    array(
		            	'focus' => "", // focus first element
                        'submit_label' => "Submit",
                        'submit_class' => "btn btn-default",
                        'inputs' => array(
                            'Street' => array(
                                    'type' => "text", // default
                                    //'required' => "true",
                                    'class' => "form-control",
                                    'placeholder'=>"Enter the street"
        				    ),
                            'House_Number' => array(
                                'type' => "text", // default
                                //'required' => "true",
                                'class' => "form-control",
                                'placeholder'=>"Enter the house Nr"
                            ),
                            'Postal_Code' => array(
                                'type' => "text", // default
                                //'required' => "true",
                                'class' => "form-control",
                                'placeholder'=>"Enter the zip code"
                            ),
                            'City' => array(
                                'type' => "text", // default
                                //'required' => "true",
                                'class' => "form-control",
                                'placeholder'=>"Enter the city"
                            ),

                        )                        
                    )                  
        );

        $forms['single_validation_form'] =   clone  $this->jform;

        // 'batch_validation_form'
        $this->jform->setForm(
            array(
                'focus' => "", // focus first element
                'submit_label' => "Submit",
                'submit_class' => "btn btn-default",
                'inputs' => array(
                    'Dataset_File' => array(
                        'type' => "file",
                        //'required' => "true",
                    ),

                )
            )
        );
        $forms['batch_validation_form'] =   clone  $this->jform;
        return $forms;
    }
}
