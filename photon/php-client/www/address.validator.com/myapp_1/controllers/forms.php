<?php

/**
 * forms.php
 *
 * forms application controller
 *
 * @package        TinyMVC
 * @author        Monte Ohrt
 */
class Forms_Controller extends Base_Controller
{
    /**
     * Standard site
     *
     * @throws Exception
     */
    public function index()
    {
        // load the model
        $this->load->model('Address_Model', 'address');
        $address = $this->address->getAddress();
        $this->tpl->assign('addresses', $address);
    }

    /**
     * Single address save to DB and create request
     *
     * @return string
     * @throws Exception
     */
    public function add()
    {
        $status = false;
        if ($this->is_post()) {
            // load the models
            $this->load->model('Address_Model', 'address');
            $this->load->model('Request_Model', 'request');
            //Todo userId
            $requestId = $this->request->createRequest(1);
            $data = $this->post();
            $data['request_id'] = $requestId;
            $addressId = $this->address->insertAddress($data);

            //request to service
            $this->load->model('AddressValidator_Model', 'addressvalidator', null, 'addressvalidator');
            try {
                $response_rpc = $this->addressvalidator->getClient()->check_address(
                    $data['street'],
                    $data['house_nr'],
                    $data['zip'],
                    $data['city'],
                    $country = 'DE',
                    $delimiter = ',',
                    $do_fuzzy = true,
                    $do_update_limit_from_cache = false
                );
                $response = array(
                    'status' => $response_rpc->getServiceStatus(),
                    'latitude' => $response_rpc->getLatLng()[0],
                    'longitude' => $response_rpc->getLatLng()[1],
                    'last_provider' => $response_rpc->getValidationResult()['last_provider'],
                    'correct_address' => isset($response_rpc->getValidationResult()['correct_address']) ? $response_rpc->getValidationResult()['correct_address'] : "",
                    'error' => $response_rpc->getValidationResult()['error'],
                );

                //Address update with response
                $response['id'] = $addressId;
                $this->address->updateAddress($response);

                //Request status update
                $data = array();
                $data['id'] = $requestId;
                $data['status'] = $response['status'];
                $data['count'] = 1;
                $requestId = $this->request->updateRequest($data);

                $status = !$response['status'];

            } catch (AddressValidator_Client_Exception_HttpException $e) {
                AddressValidator_Debugger::log($e->getMessage());
                $this->tpl->assign('addressvalidator_output', $e->getMessage());
            }
        }

        return json_encode(array('status' => $status, 'response' => implode(',', $response)));
    }

    /**
     * Single address save to DB and create request
     *
     * @return string
     * @throws Exception
     */
    public function addfile()
    {
        $status = false;
        if ($this->is_post()) {
            // load the models
            $this->load->model('Address_Model', 'address');
            $this->load->model('Request_Model', 'request');

            $target_file = $_FILES['user_file']['tmp_name'];
            $csvFile = file($target_file);

            //Todo userId
            $requestId = $this->request->createRequest(1);
            $tmp = array();
            foreach ($csvFile as $rowCSV) {
                $csvArray = explode(';', trim($rowCSV));
                //'DE';'Street_Name';'House_Nr';'ZIP';'City'
                //only correctly rows in db save
                if (count($csvArray) == 5) {
                    $data['country'] = $csvArray[0];
                    $data['street'] = $csvArray[1];
                    $data['house_nr'] = $csvArray[2];
                    $data['zip'] = $csvArray[3];
                    $data['city'] = $csvArray[4];
                    $data['request_id'] = $requestId;
                    $addressId = $this->address->insertAddress($data);
                }
            }
        }

        return json_encode(array('status' => $status, 'response' => 1));
    }
}