<?php

/**
 * test.php
 *
 * application controller to test different features
 *
 * @package             TinyMVC
 * @author              Igor Marfin
 */

class Test_Controller extends Base_Controller
{
  function index()
  {
    // any controller can use common configs of the view layers
    //include dirname(__FILE__)."/../configs/config_template_views.php";

    // load 'mytemplates' --   TinyMVC_Library_Templates with user-defined functionality.
    // not necessary to load! It is an auto-loaded module! see config_autoload.php
    //$this->load->library('templates','mytemplates');

    // 1) Test of the templates engines
    $this->tpl->assign('title', ($this->config['test']['title']));

    //  a simple  test of the TinyMVC template engine
    $this->tpl->assign('url', $this->mytemplates->anchor('test.html','This is a test'));
    $this->tpl->assign('jslog', $this->mytemplates->js_console_log('JS LOG Test!'));

    //2) Test  of the Socket.io. Please, start  bin/test_server.py in the shell
    // display the view layer with substitution templates.
    // 3 templates engine in one chain: TinyMVC->TBS->TWIG->final view
    $this->tpl->assign('test','TEST!!!!');
    $this->tpl->assign('socketio_js_path',$this->mytemplates->get_socketio_js_path());
    $this->tpl->assign('socketio_log',$this->mytemplates->add_socketio_log_html());
    $this->tpl->assign('socketio_test',$this->mytemplates->socketio_test());
    //$this->tbs->LoadTemplate('test_view'); # load a template
//    $this->tbs->Source=$this->view->fetch('test_view');
//    $this->tbs->Render = TBS_NOTHING ;
//    $this->tbs->Show();
    //$output= $this->tbs->Source;
    //echo $this->twig->render($output,array('text' => $GLOBALS['config']['test']['text']));



    // 3) Test  of the AddressValidator Client
    //load AddressValidator_Model to the controller.
    $this->load->model('AddressValidator_Model', 'addressvalidator', null, 'addressvalidator');

    // construct a client and make a few validations!
    try {
/*
        $response_rpc = $this->addressvalidator->getClient()->check_address('WILHELM STRASSE','77','53721','SIEGBURG',$country='DE',$delimiter=',',$do_fuzzy=true, $do_update_limit_from_cache=false);
        //$this->tbs->VarRef['addressvalidator_output']=$response_rpc;
        $response[] = array_merge(array('status' => $response_rpc->getServiceStatus(),'result_lat' => $response_rpc->getLatLng()[0], 'result_lng' => $response_rpc->getLatLng()[1]), $response_rpc->getValidationResult());
        $response_rpc = $this->addressvalidator->getClient()->check_address('ZAPFENDORFERSTRAßE','20','96110','SCHEßLITZ',$country='DE',$delimiter=',',$do_fuzzy=true, $do_update_limit_from_cache=false);
        $response[] = array_merge(array('status' => $response_rpc->getServiceStatus(),'result_lat' => $response_rpc->getLatLng()[0], 'result_lng' => $response_rpc->getLatLng()[1]), $response_rpc->getValidationResult());
        $response_rpc = $this->addressvalidator->getClient()->check_address('KONRAD-LANG-STRASSE','59','63128','DIETZENBACH',$country='DE',$delimiter=',',$do_fuzzy=true, $do_update_limit_from_cache=false);
        $response[] = array_merge(array('status' => $response_rpc->getServiceStatus(),'result_lat' => $response_rpc->getLatLng()[0], 'result_lng' => $response_rpc->getLatLng()[1]), $response_rpc->getValidationResult());
        $response_rpc = $this->addressvalidator->getClient()->check_address('Walldorfe str.','3a','77777','Sankt Leon-Rot',$country='DE',$delimiter=',',$do_fuzzy=true, $do_update_limit_from_cache=false);
        $response[] = array_merge(array('status' => $response_rpc->getServiceStatus(),'result_lat' => $response_rpc->getLatLng()[0], 'result_lng' => $response_rpc->getLatLng()[1]), $response_rpc->getValidationResult());       
        $response_rpc = $this->addressvalidator->getClient()->check_address('Lauenhainer ','61','09648','Mittweidu',$country='DE',$delimiter=',',$do_fuzzy=true, $do_update_limit_from_cache=false);
        $response[] = array_merge(array('status' => $response_rpc->getServiceStatus(),'result_lat' => $response_rpc->getLatLng()[0], 'result_lng' => $response_rpc->getLatLng()[1]), $response_rpc->getValidationResult());
        $response_rpc = $this->addressvalidator->getClient()->check_address('Maldorfer Str','6a','68789','Sankt Leon-Rot',$country='DE',$delimiter=',',$do_fuzzy=true, $do_update_limit_from_cache=false);
        $response[] = array_merge(array('status' => $response_rpc->getServiceStatus(),'result_lat' => $response_rpc->getLatLng()[0], 'result_lng' => $response_rpc->getLatLng()[1]), $response_rpc->getValidationResult());
*/
    } catch (AddressValidator_Client_Exception_HttpException $e) {
        AddressValidator_Debugger::log($e->getMessage());
        $this->tpl->assign('addressvalidator_output',$e->getMessage());
    }

//    $this->tbs->Source=$this->view->fetch('test_view');
//    $this->tbs->MergeBlock('response',$response);
//    $this->tbs->Show();
//    $output= $this->tbs->Source;
//    echo $this->twig->render($output,array('text' => $config['test']['text']));

//      $this->tpl->assign('responses',$response);

      return array('test_view.twig', array('title3' => 1));
  }
}
?>
