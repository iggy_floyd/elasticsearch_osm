<?php

/**
 * default.php
 *
 * default application controller
 *
 * @package		TinyMVC
 * @author		Monte Ohrt
 */

class Charts_Controller extends Base_Controller
{
  function index()
  {
    return array('charts_view.twig');
  }
}

?>
