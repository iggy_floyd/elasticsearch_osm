<?php

/**
 * tables.php
 *
 * default application controller
 *
 * @package		TinyMVC
 * @author		Monte Ohrt
 */

class Tables_Controller extends Base_Controller
{
  public function index()
  {
    // load the model
    $this->load->model('Request_Model','request');
    $requests = $this->request->getRequests();
    $this->tpl->assign('requests', $requests);
  }

  /**
   * Get request info as json
   * @throws Exception
   */
  public function getRequestInfo(){
    $tmp = explode('_',$this->get('id'));
    $id = $tmp[1];
    $this->load->model('Address_Model','address');
    $requestInfo = $this->address->getAddressinfoByRequestId($id);
    return json_encode($requestInfo);
  }
}
