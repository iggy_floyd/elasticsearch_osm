<?php

/**
 * autoload.php
 *
 * application auto-loaded plugin configuration
 *
 * @package		TinyMVC
 * @author		Monte Ohrt
 */


/* auto-loaded libraries */
$config['libraries'] = array(
array('TBS_Wrapper','tbs'),
array('TWIG','tpl'),
array('JForm_Wrapper','jform'),
array('Templates','mytemplates'),
);

/* auto-loaded scripts */
$config['scripts'] = array();
?>
