<?php

/**
 *
 *
 * configuration for view templates
 *
 * @package		TinyMVC
 */

// /test
$config['test']['title'] = 'This is a test page'; // title of the page
$config['test']['text'] = 'Hello world!';

// common settings for all routes
$config['common']['title'] = 'Address Validator - Admin Panel'; // title of the page
$config['common']['navbar']=array(
    'Dashboard'=>array(
        'name' => 'Dashboard',
        'link' => '/',
        'class' => 'fa fa-fw fa-dashboard',
        'active' => false
    ),
    'Charts'=>array(
        'name' => 'Charts',
        'link' => '/charts',
        'class' => 'fa fa-fw fa-bar-chart-o',
        'active' => false

    ),
    'Tables'=>array(
        'name' => 'Tables',
        'link' => '/tables',
        'class' => 'fa fa-fw fa-table',
        'active' => false
    ),
    'Forms'=>array(
        'name' => 'Forms',
        'link' => '/forms',
        'class' => 'fa fa-fw fa-edit',
        'active' => false
    ),
    'Test'=>array(
        'name' => 'Test',
        'link' => '/test',
        'class' => 'fa fa-fw fa-bell-slash',
        'active' => false
    ),

); // left-side the Navigation bar
$config['common']['subtitle']=''; // sub-title

// / (default)
$config['default']['title']=$config['common']['title'];
$config['default']['subtitle']='Dashboard <small>Statistics Overview</small>';
$config['default']['navbar'] =  $config['common']['navbar'];
$config['default']['navbar']['Dashboard']=array(
    'name' => 'Dashboard',
    'link' => '/',
    'class' => 'fa fa-fw fa-dashboard',
    'active' => true
);

// /forms
$config['forms']['title']=$config['common']['title'];
$config['forms']['subtitle']='Input Forms: <small>Addresses to Validation</small>';
$config['forms']['navbar'] =  $config['common']['navbar'];
$config['forms']['navbar']['Forms']=array(
    'name' => 'Forms',
    'link' => '/forms',
    'class' => 'fa fa-fw fa-edit',
    'active' => true);

// /tables
$config['tables']['title']=$config['common']['title'];
$config['tables']['subtitle']='Tables: <small>Results from  Validation</small>';
$config['tables']['navbar'] =  $config['common']['navbar'];
$config['tables']['navbar']['Tables']=array(
    'name' => 'Tables',
    'link' => '/tables',
    'class' => 'fa fa-fw fa-table',
    'active' => true);
