<?php

/*
 *  Copyright (c) 2014, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */

/**
 * AddressValidator_Result represents a response from ElasticOSM Service
 *
 * @author Igor Marfin
 */
class AddressValidator_Result extends ArrayIterator
{

    private $_result = null;
    private $_filtered = null;

    public function __construct($result)
    {
        if (array_key_exists('features', (array) $result)) {
            //$this->_result = json_decode($result['result'],true);
            $this->_result = $result['features'];
            return parent::__construct((Array) $this->_result);
        } else {
            $this->_result = array();
            return parent::__construct(array());
        }
    }

    public function filter($key,$value) {
        $this->_filtered=array_filter($this->_result, function($v) use($key,$value) { return in_array($v["properties"][$key], $value,true); });
        return $this;
    }

    public function is_filtered(){
        return isset($this->_filtered);
    }

    public function streets(){
        if ($this->is_filtered()) {
            $_filtered_streets= array_filter($this->_filtered, function($v)  { return array_key_exists('street',$v["properties"]); });
            $_streets = [];
            array_walk($_filtered_streets,function($v) use(&$_streets) {  $_streets[] = $v['properties']['street']; });
            return  $_streets;
        }
        return  array();
    }

    public function housenumbers(){
        if ($this->is_filtered()) {
            $_filtered_housenumbers= array_filter($this->_filtered, function($v)  { return array_key_exists('housenumber',$v["properties"]); });
            $_housenumbers = [];
            array_walk($_filtered_housenumbers,function($v) use(&$_housenumbers) {  $_housenumbers[] = $v['properties']['housenumber']; });
            return  $_housenumbers;
        }
        return array();
    }


    public function postcodes(){
        if ($this->is_filtered()) {
            $_filtered_postcodes= array_filter($this->_filtered, function($v)  { return array_key_exists('postcode',$v["properties"]); });
            $_postcodes = [];
            array_walk($_filtered_postcodes,function($v) use(&$_postcodes) {  $_postcodes[] = $v['properties']['postcode']; });
            return  $_postcodes;
        }
        return  array();
    }


    public function cities(){
        if ($this->is_filtered()) {
            $_filtered_cities= array_filter($this->_filtered, function($v)  { return array_key_exists('city',$v["properties"]); });
            $_cities = [];
            array_walk($_filtered_cities,function($v) use(&$_cities) {  $_cities[] = $v['properties']['city']; });
            return  $_cities;
        }
        return  array();
    }

    public function coordinates(){
        if ($this->is_filtered()) {
            $_filtered_coordinates= array_filter($this->_filtered, function($v)  { return array_key_exists('coordinates',$v["geometry"]); });
            $_coordinates = [];
            array_walk($_filtered_coordinates,function($v) use(&$_coordinates) {  $_coordinates[] = $v['geometry']['coordinates']; });
            return  $_coordinates;
        }
        return  array();
    }

    public function __toString()
    {
        return json_encode($this->_result);
    }

}
