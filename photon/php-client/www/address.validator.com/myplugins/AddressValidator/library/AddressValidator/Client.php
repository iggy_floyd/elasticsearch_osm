<?php

class AddressValidator_Client
{
    /* @var URL_READ_GREMLIN  AddressValidator_Client */
    const URL_COMMUNICATE = 'api/';

    
    /* @var $_adapter AddressValidator_Client */
    private $_adapter = null;

 
    /**
     * @param Cayley_Client_Adapter_Interface $adapter_w
     */

    public function __construct(AddressValidator_Client_Adapter_Interface $adapter)
    {
        $this->_adapter = $adapter;
        set_time_limit(0); // to infinity
    }



    /** performs the check of the addresses
     *
     *
     * @param string $street street name
     * @param string $house   house Nr
     * @param string $plz     zipcode
     * @param string $city    city name
     * @param string $country  country name
     * @param string $delimiter  delimiter to separate fields in the search string
     *
     * @return AddressValidator_Result
     */

    public function checkAddress($street,$house,$plz,$city,$country,$delimiter) {
        $args = func_get_args();
        $address = implode(array_pop($args),$args);
        return $this->execute($address);
    }




    /**
     * executes a method
     * @param  string         $address
     * @return AddressValidator_Result
     */

    public function execute($address)
    {
        $request = array(
            'q' => $address
        );
        $response = $this->_doRequest($this->_adapter, self::URL_COMMUNICATE, $request,'GET');
        return new AddressValidator_Result(json_decode($response, true));

    }


    /**
     * sends a query to the service
     * @param  AddressValidator_Client_Adapter_Interface $client
     * @param  string $url
     * @param  string $body
     * @param string $type
     * @throws AddressValidator_Client_Exception_HttpException
     * @return string
     */

    private function _doRequest(AddressValidator_Client_Adapter_Interface $client, $url, $body,$type='POST')
    {
        try {
            return $client->query($url, $body,$type);
        } catch (Exception $e) {
            throw new AddressValidator_Client_Exception_HttpException($e);
        }
    }

}
