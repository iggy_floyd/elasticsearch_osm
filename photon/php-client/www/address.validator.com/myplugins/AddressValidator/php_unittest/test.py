#! /usr/bin/env python

'''
#
#  Copyright (c) 2014, < Igor Marfin <igor.marfin@unister.de> >
#  All rights reserved.
#
#
#  Usage: %(scriptName)s
#
'''


from subprocess import Popen,PIPE
import inspect
import sys


def test_php(file):

 file=file.replace("_","/");
 return Popen(["php", file], stdout=PIPE).communicate()[0][:-1]
  


def Result():
   """ Result.php

   >>> Result()
   'string(114) "{"status":1,"result":{"reason":"test_error","error":1,"result":["1","1"],"last_provider":""},"error":"test_error"}"'
   """
   return test_php(str(inspect.stack()[0][3])+".php")


def Debugger():
   """  Debugger.php

   >>> Debugger()
   'bool(true)'
   """
   return test_php(str(inspect.stack()[0][3])+".php")


def Client():
   """  Client.php

   >>> Client()
   'string(109) "{"last_provider":"Osm","reason":"","correct_address":"","result":["50.7967605","7.20251030188577"],"error":0}"'
   """
   return test_php(str(inspect.stack()[0][3])+".php")


def Client_HttpException():
   """  Client/HttpException.php

   >>> Client_HttpException()
   'string(25) "{"error":"Error message"}"'
   """
   return test_php(str(inspect.stack()[0][3])+".php")




if __name__ == '__main__':

    print __doc__ % {'scriptName' : sys.argv[0]}


    import doctest
    doctest.testmod(verbose=True)
