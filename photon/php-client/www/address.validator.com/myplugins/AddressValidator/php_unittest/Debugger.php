<?php

/* 
 *  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */

include_once dirname(__FILE__) . '/../SplClassLoader.php';


 $classLoader = new SplClassLoader(null, dirname(__FILE__)."/../library/");
 $classLoader->register();
 
 $qr = new AddressValidator_Debugger();
 $qr::reporting(E_ALL);
 $qr::logging(E_ALL);
 var_dump(E_ALL==$qr::logging());
 
 