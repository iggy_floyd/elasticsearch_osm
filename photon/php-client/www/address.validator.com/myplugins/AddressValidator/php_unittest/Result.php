<?php

/* 
 *  Copyright (c) 2014, <Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */

include_once dirname(__FILE__) . '/../SplClassLoader.php';

$classLoader = new SplClassLoader(null, dirname(__FILE__)."/../library/");
$classLoader->register();

$result = json_decode('{ "jsonrpc": "2.0", "id": "1","result":{"status":1,"result":{"reason":"test_error","error":1,"result":["1","1"],"last_provider":""},"error":"test_error"}}',True);
$qr = new AddressValidator_Result($result);
var_dump(json_encode($qr));




