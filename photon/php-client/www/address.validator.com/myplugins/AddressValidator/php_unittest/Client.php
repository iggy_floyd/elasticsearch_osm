<?php

/* 
 *  Copyright (c) 2014, < Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */


/**
 * to test reading cayley database
 * first, start the cayley:  cd bk-tree-0.1/cayley/cayley_0.4.0_linux_386; ./cayley http --dbpath=testdata.nq
 */


include_once dirname(__FILE__) . '/../SplClassLoader.php';


$classLoader = new SplClassLoader(null, dirname(__FILE__)."/../library/");
$classLoader->register();
 
$config = parse_ini_file("config/config.ini");
$adapter_http=AddressValidator_Client_Adapter_Http::connect($config['host'], $config['port'])
    ->setHeaders(array('Content-Type: text/plain'));

$client = new AddressValidator_Client($adapter_http);
$result =  new AddressValidator_Result($client->check_address('WILHELM STRASSE','77','53721','SIEGBURG',$country='DE',$delimiter=',',$do_fuzzy=true, $do_update_limit_from_cache=false));
var_dump(json_encode($result));
