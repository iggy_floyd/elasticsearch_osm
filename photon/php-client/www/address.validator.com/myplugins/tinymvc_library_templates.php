<?php

class TinyMVC_Library_Templates {

  /*
   * for test purposes
   */
  function test()
  {
    return "This is a test.";
  }

  /*
   * makes escaping the string
  */
 function esc($string)
 {
  return htmlentities($string);
 }

 /*
  * produces the anchor
  */
 function anchor($url,$text)
 {
  return "<a href=\"$url\">$text</a>";
 }

    /*
     * produces the logging of the $text in the console for debugging purpose
     */
   function  js_console_log($text) {
    $jsfunc = <<<EOT

    <script type="text/javascript">
    console.log("$text");
    </script>
EOT;
    return $jsfunc;
   }

    /*
     * returns the socket.io library path
     */
    function  get_socketio_js_path() {
        $jsfunc = <<<EOT
<script src="js/socket.io.js"></script>
<!--
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/0.9.16/socket.io.min.js"></script>
-->
EOT;
        return $jsfunc;
    }

    /*
     * adds logging support to the html for testing socket,io functionality
     */
    function  add_socketio_log_html() {
        $jsfunc = <<<EOT
     <h2>Received:</h2>
     <div><p id="log"></p></div>
EOT;
        return $jsfunc;
    }

    /*
     * adds logging support to the html for testing socket,io functionality
     */
    function  socketio_test() {
        $jsfunc = <<<EOT
            namespace = '/test';

            var socket = io.connect('http://' + 'localhost' + ':' + 8432 + namespace);
            socket.on('connect', function() {
                console.log('Connected!');
                socket.emit('server_request', {data: 'I\'m connected!'});
                $('#log').append('<br>Connected');
            });
            socket.on('disconnect', function() {
                $('#log').append('<br>Disconnected');
            });
            socket.on('server_response', function(msg) {
                console.log(msg);
                $('#log').append('<br>Received:' + msg.data);

            });

EOT;
        return $jsfunc;
    }
}

?>
