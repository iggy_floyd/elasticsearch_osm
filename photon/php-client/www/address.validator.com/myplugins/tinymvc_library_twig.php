<?php

require_once dirname(__FILE__) . '/Twig-1.23.1/lib/Twig/Autoloader.php';

//Twig_Autoloader::register();

class TinyMVC_Library_TWIG
{
    protected $vars = array();

    protected $templatesDirectory = '';

    protected $cacheDirectory = '/tmp';

    public $twig;

    public function __construct()
    {
        Twig_Autoloader::register();
        $this->setEnvironment();
        $loader = new Twig_Loader_Filesystem($this->templatesDirectory);
        $this->twig = new Twig_Environment($loader, array('cache' => $this->cacheDirectory,'auto_reload' =>true));
    }

    /**
     * Set TPL Environment
     * Todo: Read config
     */
    private function setEnvironment()
    {

        if (defined('TWIG_VIEWS')) {
            $this->templatesDirectory =TWIG_VIEWS . 'views/';
        } else {
            $this->templatesDirectory = dirname(__FILE__) . '/../views/';
        }

    }

    public function assign($variable, $value)
    {
        $this->vars[$variable] = $value;
    }

    public function getVars()
    {
        return $this->vars;
    }

    public function setVars($vars)
    {
        if (is_array($vars)) {
            $this->vars = array_merge($this->vars, $vars);
        }

    }

    function loadTemplate($tname)
    {
        return $this->twig->loadTemplate($tname);
    }
}


?>
