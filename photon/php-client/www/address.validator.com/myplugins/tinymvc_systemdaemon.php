<?php

/* 
 *  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */

/* * *
 * Name:       TinyMVC_AddressValidator
 * * */

// ------------------------------------------------------------------------
if (!class_exists('SplClassLoader')) {
    include_once dirname(__FILE__) . '/SystemDaemon/SplClassLoader.php';
}


/*
 * A class which allows to add methods to objects on-fly
 *
 */
class stdObject {
    public static $status;
    public function __construct(array $arguments = array()) {
        //$this->status=true;
        if (!empty($arguments)) {
            foreach ($arguments as $property => $argument) {
                if ($argument instanceOf Closure) {
                    $this->{$property} = $argument;
                } else {
                    $this->{$property} = $argument;
                }
            }
        }
    }

    public function __call($method, $arguments) {
        if (isset($this->{$method}) && is_callable($this->{$method})) {
            return call_user_func_array($this->{$method}, $arguments);
        } else {
            throw new Exception("Fatal error: Call to undefined method stdObject::{$method}()");
        }
    }
    function __toString(){ return 'this'; } // test for reference
}
stdObject::$status=true;
/*
 * Here is an example of one way to define, then use the variable ( $this ) in Closure functions.
 *
 */


function requesting_class($object_proprty=null)
{
    foreach(debug_backtrace(true) as $stack){
//        echo "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB\n".PHP_EOL;
//        echo var_dump($stack);
        if(isset($stack['object'])){
            if (isset($object_proprty) && isset($stack['object']->{$object_proprty})) {
//                echo "22222222222222222222\n".PHP_EOL;
//                echo var_dump($stack['object']->{$object_proprty});
                return $stack['object']->{$object_proprty};
            } else {
        //        echo "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\n".PHP_EOL;
                return $stack['object'];
            }

        }

    }

}

/*
 // Test of the 'stdObject'
$person = new stdObject(array(
    "name" => "nick",
    "age" => 23,
    "friends" => array("frank", "sally", "aaron"),
    "sayHi" => function() {

            //echo $this; // Notice: Undefined variable: this
            //$this = new Class() // FATAL ERROR
            // Trick to assign the variable '$this'
            extract(array('this' => requesting_class())); // Determine what class is responsible for making the call to Closure
            //var_dump( $this  );  // Passive reference works
            //var_dump( $$this ); // Added to class:  function __toString(){ return 'this'; }
            //$name = $$this->name;
            //echo $$this->name;
            //echo $$this->age;
            //var_dump($$this->friends);
            return "Hello ". $$this->name;
        }

));

$person->sayHi2 = function() {
    return "Hello there 2";
};

echo $person->sayHi();
echo $person->sayHi2();
*/

/*
 *
 *  The Lock mechanism
 * @author Igor Marfin
 * @return boolean
 */
function getLock($lockFile,$createlock=true)
{
/*
    #   If a lock file doesn't  exist, create it and  return TRUE
    if (@symlink("/proc/" . getmypid(), $lockFile) !== FALSE){ # the @ in front of 'symlink' is to suppress the NOTICE you get if the LOCK_FILE exists
        return true; # we were able to create a symlink to our /proc/pid file
    }
*/
    if (!file_exists($lockFile)) {
        if ($createlock) {
            fopen($lockFile, 'wb');
        }
        return true;
    }

    # if the link already exists
    return false;
}

function removeLock($lockFile) {
/*
    # check if the link is stale
    if (is_link($lockFile) && !is_dir($lockFile)) {
        # if it's not stale: daemon is still running
        # remove the link and return true
        @unlink($lockFile);
        return true;
    }
    return false;
*/
    if (file_exists($lockFile)) {
        @unlink($lockFile);
        return true;
    }
    return false;
}


/**
 * 
 * Wrapper of the system daemon support
 *
 * @author Igor Marfin
 */
class TinyMVC_SystemDaemon {

    /**
     * 
     *
     * $client handle
     *
     * @access	public
     */
    public $daemon = null;
    public $classLoader = null;

    /**
     * class constructor
     *
     * @access	public
     */
    function __construct($config) {


        $this->classLoader = new SplClassLoader(null, dirname(__FILE__) . "/SystemDaemon/");
        $this->classLoader->register();
        if(!class_exists('System_Daemon',true)) {
            throw new Exception("PHP  SystemDaemon package is required.");
        }


        if ($config['debug']) {
            error_reporting(E_ALL);
        } else {
            error_reporting(null);
        }

        // here we declare and define the Daemon Worker instance:
        $this->daemon = new stdObject(array(

            // options of the deamon
            'options'=> array(
                'appName' => 'simple',
                'appDir' =>'/tmp',
                'appDescription' => 'It does a test',
                'authorName' => 'Igor Marfin',
                'authorEmail' => 'igor.marfin.@example.net',
                'sysMaxExecutionTime' => '0',
                'sysMaxInputTime' => '0',
                'sysMemoryLimit' => '1024M',
                'logLocation' =>  '/tmp/sysdaemon.log',
                'logFilePosition' => true,
                'usePEAR' => false,
                'appPidLocation' =>'/tmp/simple/simple.pid',
                'appDieOnIdentityCrisis' => false,
                'logPhpErrors' =>true,
                'log_echoed' =>false
            ),
            //do we spawn?
            'spawned' => true,
            // the main method to start the daemon
            'start' =>function() {
                    extract(array('this_daemon' => requesting_class('daemon'))); // Determine what class is responsible for making the call to Closure
                    System_Daemon::setOptions($this_daemon->options);
                    // Spawn Deamon!
                    //System_Daemon::log(System_Daemon::LOG_INFO,"8888888");
                    //$this_daemon->parent->load->model('Address_Model', 'address');
                    //System_Daemon::log(System_Daemon::LOG_INFO,"7777");
   //                 ADD NON-SPAWNED DAEMONS!!!!
                    if ($this_daemon->spawned) {
                        if (System_Daemon::isRunning()) {
                            return;
                        }
                        System_Daemon::start();
                        //System_Daemon::log(System_Daemon::LOG_INFO,"9999");
                        // the working code (loop) of the daemon is running here
                        $this_daemon->run();
                    } else {
                        if(!getLock($this_daemon->options['appPidLocation'])) {
                            System_Daemon::log(System_Daemon::LOG_ERR,"We are already started. Exit");
                            return;
                        }
                        System_Daemon::log(System_Daemon::LOG_INFO,"We are started in the non-spawned mode");
                        $this_daemon->run($this_daemon->options);
                        System_Daemon::log(System_Daemon::LOG_INFO,"We have done in the non-spawned mode");
                    }

                    // Shut down the daemon nicely
                    // This is ignored if the class is actually running in the foreground
                   if ($this_daemon->spawned) {
                    System_Daemon::stop();
                   } else {
                       removeLock($this_daemon->options['appPidLocation']);
                   }
                },
            'test' => function () {
                        extract(array('this_daemon' => requesting_class('daemon')));
                        var_dump($this_daemon->parent);
                        //var_dump($$this_daemon);
                        //var_dump($this->daemon);
                       },
            'stop' => function () {
                        extract(array('this_daemon' => requesting_class('daemon'))); // Determine what class is responsible for making the call to Closure
                        if ($this_daemon->spawned) {
                            System_Daemon::setOptions($this_daemon->options);
                            if (System_Daemon::isRunning()) {
                                System_Daemon::log(System_Daemon::LOG_ERR,"Stopping now!");
//                                System_Daemon::stop();
                                $pid = file_get_contents(
                                    System_Daemon::getOption('appPidLocation')
                                );
                                System_Daemon::log(System_Daemon::LOG_INFO,"The process with pid ".$pid." will be killed");
                                @unlink(System_Daemon::getOption('appPidLocation'));
                                passthru('kill -9 ' . $pid);
                                die();
                            }
                        } else {
                            System_Daemon::info(System_Daemon::LOG_INFO,"Removing Lock...");
                            removeLock($this_daemon->options['appPidLocation']);
                        }
                    }

        ));

        /**
         *
         * here some sanity checks are performed
         *
         * This is an example how we can add the worker run function!
         */
 /*
        $this->daemon->run = function() {
            // This variable keeps track of how many 'runs' or 'loops' your daemon has
            // done so far. For example purposes, we're quitting on 3.
            $cnt = 1;

            // This variable gives your own code the ability to breakdown the daemon:
            $runningOkay = true;

            while (!System_Daemon::isDying() && $runningOkay && $cnt <=3) {
                // What mode are we in?
                $mode = '"'.(System_Daemon::isInBackground() ? '' : 'non-' ).
                    'daemon" mode';

                System_Daemon::log(System_Daemon::LOG_INFO, "Daemon: '".
                    System_Daemon::getOption("appName").
                    "' spawned! This will be written to ".
                    System_Daemon::getOption("logLocation"));

                System_Daemon::info('{appName} running in %s %s/3',
                    $mode,
                    $cnt
                );

                //In the actuall logparser program, You could replace 'true'
                // With e.g. a  parseLog('vsftpd') function, and have it return
                // either true on success, or false on failure.
                extract(array('this_daemon' => requesting_class('daemon'))); // Determine what class is responsible for making the call to Closure
                System_Daemon::log(System_Daemon::LOG_INFO,"1 Daemon status ? -->".stdObject::$status);
                System_Daemon::log(System_Daemon::LOG_INFO,"2 Daemon has stop ? -->".isset($this_daemon->stopflag));
                System_Daemon::log(System_Daemon::LOG_INFO,"3 Daemon has stop ? -->".isset($this->stopflag));
                $runningOkay = isset($this_daemon->stopflag) ||  isset($this->stopflag) ? false:true;


                // Level 4 would be fatal and shuts down the daemon immediately,
                // which in this case is handled by the while condition.
                if (!$runningOkay) {
                    System_Daemon::err('daemon produced an error, '.
                        'so this will be my last run');
                }

                // Relax the system by sleeping for a little bit
                // iterate also clears statcache
                System_Daemon::iterate(10);

                $cnt++;
            }

        };

        // let's start the daemon
        //$this->daemon->stopflag=true;
        //$this->stopflag=true;
//        stdObject::$status='NOT_OK';
        $this->daemon->start();
//        sleep(1);
//        echo "STOPING!!!";
//        stdObject::$status='OK';
//        $this->daemon->stop();
//        $this->daemon->stopflag=true;
//        $this->stopflag=true;

        //$this->daemon->test();
*/
        /*
         *
         * An example of the use (see more examples in SystemDaemon/examples)
         *
         */

        // Bare minimum setup
//        System_Daemon::setOption("appName", "simple");
//        System_Daemon::setOption("authorEmail", "igor.marfin.@example.net");
//        System_Daemon::setOption('logLocation', '/tmp/sysdaemon.log');
//        System_Daemon::setOption('logFilePosition', true);
//        System_Daemon::setOption("appDir", dirname(__FILE__));

        // Extended setup
/*

        $options = array(
            'appName' => 'simple',
            'appDir' =>'/tmp',
            'appDescription' => 'It does a test',
            'authorName' => 'Igor Marfin',
            'authorEmail' => 'igor.marfin.@example.net',
            'sysMaxExecutionTime' => '0',
            'sysMaxInputTime' => '0',
            'sysMemoryLimit' => '1024M',
//            'appRunAsGID' => 1000, // vagrant user
//            'appRunAsUID' => 1000, // vagrant group
//            'logLocation' =>  '/home/vagrant/project/www/address.validator.com/logs/sysdaemon.log',
            'logLocation' =>  '/tmp/sysdaemon.log',
            'logFilePosition' => true,
            'usePEAR' => false,
            'appPidLocation' =>'/tmp/simple/simple.pid',
            'appDieOnIdentityCrisis' => false
        );
        System_Daemon::setOptions($options);

        //BEGIN:The body of our daemon
        System_Daemon::log(System_Daemon::LOG_INFO, "Daemon not yet started so ".
            "this will be written on-screen");

        // Spawn Deamon!
        System_Daemon::start();

        // This variable keeps track of how many 'runs' or 'loops' your daemon has
        // done so far. For example purposes, we're quitting on 3.
        $cnt = 1;

        // This variable gives your own code the ability to breakdown the daemon:
        $runningOkay = true;

        while (!System_Daemon::isDying() && $runningOkay && $cnt <=3) {
            // What mode are we in?
            $mode = '"'.(System_Daemon::isInBackground() ? '' : 'non-' ).
                'daemon" mode';

            System_Daemon::log(System_Daemon::LOG_INFO, "Daemon: '".
                System_Daemon::getOption("appName").
                "' spawned! This will be written to ".
                System_Daemon::getOption("logLocation"));

            System_Daemon::info('{appName} running in %s %s/3',
                $mode,
                $cnt
            );

            //In the actuall logparser program, You could replace 'true'
            // With e.g. a  parseLog('vsftpd') function, and have it return
            // either true on success, or false on failure.
            $runningOkay = true;

            // Level 4 would be fatal and shuts down the daemon immediately,
            // which in this case is handled by the while condition.
            if (!$runningOkay) {
                System_Daemon::err('daemon produced an error, '.
                    'so this will be my last run');
            }

            // Relax the system by sleeping for a little bit
            // iterate also clears statcache
            System_Daemon::iterate(2);

            $cnt++;

        }

        // Shut down the daemon nicely
        // This is ignored if the class is actually running in the foreground
        System_Daemon::stop();
        //END:The body of our daemon
*/
    }
}
    
