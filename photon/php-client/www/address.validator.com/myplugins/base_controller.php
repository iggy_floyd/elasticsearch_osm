<?php
/**
 * Created by PhpStorm.
 * User: Serg
 * Date: 20.12.2015
 * Time: 22:34
 */

class Base_Controller extends TinyMVC_Controller
{
    //Global config
    protected $config;

    protected $class;

    protected $vars = array();

    protected $view_template;

    // Todo Class Request define
    private $request_object = array();

    function __construct()
    {
        parent::__construct();

        // global config to controllers
        $tmvc = tmvc::instance();
        $this->config = $tmvc->config;
        $this->class = get_called_class();

        //request
        $this->request_object['is_post'] = ('POST' == $_SERVER['REQUEST_METHOD']);
        //Todo filtering
        $this->request_object['post'] = $_POST;
        //Todo filtering
        $this->request_object['get'] = $_GET;

        //Default logic
        $base_name = strtolower(str_replace('_Controller','',$this->class));
        $this->view_template=$base_name.'_view.twig';
        if(isset($this->config[$base_name])) {
            $this->vars = $this->config[$base_name];
        }
    }

    /**
     *
     * @param null $responseFromUserController
     */
    public function postDispatch($responseFromUserController = null)
    {
        //Json?
        if( false === empty($responseFromUserController) && $this->is_json($responseFromUserController)){
            echo $responseFromUserController;
            return;
        }

        if(false === empty($responseFromUserController)) {
            if(is_array($responseFromUserController)){
                //Templatename
                if(isset($responseFromUserController[0])){
                    $this->view_template = $responseFromUserController[0];
                }

                //Variable
                if( isset($responseFromUserController[1]) && is_array($responseFromUserController[1])){
                    $this->tpl->setVars (array_merge($this->tpl->getVars(),$responseFromUserController[1]));
                }
            }
        }

        $this->vars = array_merge($this->vars, $this->tpl->getVars());
        echo $this->tpl->twig->render($this->view_template, $this->vars);
    }


    /**
     * Examine string, return true , if string is a valid json
     *
     * @param $string
     * @return bool
     */
    public function is_json($string) {
        if(is_array($string)){
            return false;
        }
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE );
    }

    public function is_post(){
        return $this->request_object['is_post'];
    }

    public function post($variable = null){
        if(false === is_null($variable)){
            return isset($this->request_object['post'][$variable])?$this->request_object['post'][$variable]:false;
        }
        return $this->request_object['post'];
    }

    public function get($variable = null){
        if(false === is_null($variable)){
            return isset($this->request_object['get'][$variable])?$this->request_object['get'][$variable]:false;
        }
        return $this->request_object['get'];
    }
}