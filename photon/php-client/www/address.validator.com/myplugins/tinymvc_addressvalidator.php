<?php

/* 
 *  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
 *  All rights reserved.
 */

/* * *
 * Name:       TinyMVC_AddressValidator
 * * */

// ------------------------------------------------------------------------
if (!class_exists('SplClassLoader')) {
    include_once dirname(__FILE__) . '/AddressValidator/SplClassLoader.php';
}

/**
 * 
 * Wrapper of the Communicator with AddressValidator
 *
 * @author Igor Marfin
 */
class TinyMVC_AddressValidator {

    /**
     * 
     *
     * $client handle
     *
     * @access	public
     */
    public $client = null;
    public $classLoader = null;

    /**
     * class constructor
     *
     * @access	public
     */
    function __construct($config) {


        $classLoader = new SplClassLoader(null, dirname(__FILE__) . "/AddressValidator/library/");
        $classLoader->register();
        /**
         * 
         * here some sanity checks are performed
         * 
         */
        if (empty($config))
            throw new Exception("database definitions required.");
        
        
        if(!class_exists('AddressValidator_KLogger',true)) {
		throw new Exception("PHP AddressValidator package is required.");
        }
        if(!class_exists('AddressValidator_Debugger',true)) {
		throw new Exception("PHP AddressValidator package is required.");
        }
        if(!class_exists('AddressValidator_Result',true)) {
		throw new Exception("PHP AddressValidator package is required.");
        }        
        if(!class_exists('AddressValidator_Client_Adapter_Http',true)) {
		throw new Exception("PHP AddressValidator package is required.");
        }


        // to start the AddressValidator_Debugger  to report any problems
        // for debugging purpose, it will be removed in the release: REPORT ALL!
        if ($config['debug']) {
            AddressValidator_Debugger::reporting( E_ALL );
        } else {
            AddressValidator_Debugger::reporting( null ); // Report NOTHING!
            AddressValidator_Debugger::logging( null );
        }

        //
        try {
            $this->client = new AddressValidator_Client(
                        AddressValidator_Client_Adapter_Http::connect($config['host'], $config['port'])->setHeaders(array('Content-Type: text/plain'))
                    );
            } catch (Exception $e) {
            throw new Exception(sprintf("Can't connect to the Address Validator.  Error: %s", $e->getMessage()));
        }



        /*
        * Here some tests of the classes of AddressValidators are defined.
        *
        * It's recommended to perform them in the 'php_unittest' test suite. However, if you are willing, you can
        * uncomment the following lines to test some features immediately.
        *
        */

/*
        try {
        AddressValidator_Debugger::log('Test 1: test of the Http adapter: check_address');
        $request = array(
            'q' => 'Gustav-Adolf-Str 34 09116 Chemnitz DE'
        );

        addressValidator_Debugger::log( json_decode(AddressValidator_Client_Adapter_Http::connect($config['host'], $config['port'])
            ->setHeaders(array('Content-Type: text/plain'))
            ->doGet(AddressValidator_Client::URL_COMMUNICATE, $request),true));


        AddressValidator_Debugger::log('Test 2: test of the Client: is_filtered');
        addressValidator_Debugger::log($this->client->checkAddress('Gustav-Adolf-Str','35','09116','','DE'," ")
        ->is_filtered());

        AddressValidator_Debugger::log('Test 3: test of the Client: checkAddress & filter');
        addressValidator_Debugger::log($this->client->checkAddress('Gustav-Adolf-Str','35','09116','','DE'," ")
            ->filter("osm_key",array('building'))
        );


        AddressValidator_Debugger::log('Test 4: test of the Client: checkAddress & streets');
        addressValidator_Debugger::log($this->client->checkAddress('Gustav-Adolf-Str','35','09116','','DE'," ")
            ->filter("osm_key",array('shop','building'))
            ->streets()
        );

        AddressValidator_Debugger::log('Test 5: test of the Client: checkAddress & housenumbers');
        addressValidator_Debugger::log($this->client->checkAddress('Gustav-Adolf-Str','35','09116','','DE'," ")
                ->filter("osm_key",array('shop','building'))
                ->housenumbers()
        );


        AddressValidator_Debugger::log('Test 6: test of the Client: checkAddress & postcodes');
        addressValidator_Debugger::log($this->client->checkAddress('Gustav-Adolf-Str','35','09116','','DE'," ")
                ->filter("osm_key",array('shop','building'))
                ->postcodes()
        );


        AddressValidator_Debugger::log('Test 7: test of the Client: checkAddress & cities');
        addressValidator_Debugger::log($this->client->checkAddress('Gustav-Adolf-Str','35','09116','','DE'," ")
                ->filter("osm_key",array('shop','building'))
                ->cities()
        );

        AddressValidator_Debugger::log('Test 8: test of the Client: checkAddress & coordinates');
        addressValidator_Debugger::log($this->client->checkAddress('Gustav-Adolf-Str','35','09116','','DE'," ")
                ->filter("osm_key",array('shop','building'))
                ->coordinates()
        );

        } catch (Exception $e) {
            AddressValidator_Debugger::log($e);
        }
*/

    }
}
    
