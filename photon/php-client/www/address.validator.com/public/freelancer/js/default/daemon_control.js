$(document).ready(function(){
/*
    $( "#daemon" ).submit(function( event ) {
        event.preventDefault();
        $.post( "default/daemon",
            $(this).serialize(),
            //{checked: $("[name='sysdaemon']:checked").val()},
            function( data ) {
                console.log(data);

            if( data.status == true ){
                $('#modal_layer').html(data.response);
                $('#myModal').modal('show');
            }else{
                $('#modal_layer').html("Error!");
                $('#myModal').modal('show');
            }
        }, "json" );
    });
*/

    $( "#daemon input" ).jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            // Prevent spam click and default submit behaviour
            event.preventDefault();
            /*
            var val =  $("input#sysdaemonInput").val();
            var checked =  $("input#sysdaemonInput").checked;
            console.log($('input[name=sysdaemon]:checked'));
            console.log("val->",val);
            console.log("checked->",checked);
            */
            $.ajax({
                url: "default/mydaemon",
                type: "POST",
                data: {
                    start: $('input[name=sysdaemon]:checked').length > 0
                },
                dataType: 'json',
                cache: false,
                success: function(data) {
//                    location.reload();
                    if( data.status == true ){
                        $('#modal_layer').html(data.response);
                        $('#myModal').modal('show');

                    }else{
                        $('#modal_layer').html("Error!");
                        $('#myModal').modal('show');
                    }

                }

            });
        }

      });

    $('#myModal').click(function() {
        location.reload();
    });

 });