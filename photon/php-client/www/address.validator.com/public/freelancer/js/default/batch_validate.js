$(document).ready(function(){

    function progressHandlingFunction(e){
        if(e.lengthComputable){
            $('progress').attr({value:e.loaded,max:e.total});
        }
    }

        var input = document.getElementById("file");
        var formdata = false;
        if (window.FormData) {
            formdata = new FormData();
            //var formdata = new FormData($('#filesend'));
        }
        input.addEventListener("change", function (evt) {
            var i = 0, len = this.files.length, img, reader, file;
            for ( ; i < len; i++ ) {
                file = this.files[i];
                //console.log(file);
                if (!!file.type.match(/text.*/)) {
                    if ( window.FileReader ) {
                        reader = new FileReader();
                        reader.onloadend = function (e) {
                            //showUploadedItem(e.target.result, file.fileName);
                        };
                        reader.readAsDataURL(file);
                    }
                    if (formdata) {
//                        console.log(file);
                        formdata.append("user_file", file);
                        formdata.append("extra",'extra-data');
                        //$('div#response').html('<br /><img src="/images/loader.gif"/>');
                        $.ajax({
                            url: "default/addfile",
                            type: "POST",
                            xhr: function() {  // Custom XMLHttpRequest
                                var myXhr = $.ajaxSettings.xhr();
                                if(myXhr.upload){ // Check if upload property exists
                                    myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                                }
                                return myXhr;
                            },
                            data: formdata,
                            processData: false,
                            contentType: false,
                            success: function (res) {
                                jQuery('div#response').html("Successfully uploaded");
                                location.reload();
                            }
                        });
                    }
//                    console.log(formdata);

                }else
                {
                    alert('Not a vaild text file!');
                }

            }
        });

    });