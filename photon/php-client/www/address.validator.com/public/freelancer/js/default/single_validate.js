$(document).ready(function(){
// method 1
    /*
     $( "#target" ).submit(function( event ) {
     event.preventDefault();
     $.post( "default/add", $(this).serialize(),function( data ) {
     if( data.status == true ){
     $('#modal_layer').html(data.response);
     $('#myModal').modal('show');
     }else{
     $('#modal_layer').html("Error!");
     $('#myModal').modal('show');
     }
     }, "json" );
     });
     */

// method 2
    $( "#target input" ).jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            // Prevent spam click and default submit behaviour
            event.preventDefault();

            var street =  $("input#streetInput").val();
            var house_nr =  $("input#houseNumberInput").val();
            var zip = $("input#postalCodeInput").val();
            var city = $("input#cityInput").val();

            $.ajax({
                url: "default/add",
                type: "POST",
                data: {
                    street: street,
                    house_nr : house_nr,
                    zip: zip,
                    city: city
                },
                dataType: 'json',
                cache: false,
                success: function(data) {
//                    location.reload();
                    if( data.status == true ){
                        $('#modal_layer').html(data.response);
                        $('#myModal').modal('show');

                    }else{
                        $('#modal_layer').html("Error!");
                        $('#myModal').modal('show');
                    }

                }
            });

        }
    });

    $('#myModal').click(function() {
        location.reload();
    });
});