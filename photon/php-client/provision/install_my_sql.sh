#!/bin/bash

export TERM=xterm
sudo apt-get -qq update
echo "mysql-server-5.5 mysql-server/root_password password dev" | sudo debconf-set-selections
echo "mysql-server-5.5 mysql-server/root_password_again password dev" | sudo debconf-set-selections
sudo apt-get -y install mysql-server-5.5  dpkg-dev php5 php5-mysql
sudo apt-get -y install libmysqlclient-dev
service mysql restart
#sudo /etc/init.d/mysql restart


# Here we create a new database

# what we expect
#$config['default']['plugin'] = 'TinyMVC_PDO'; // plugin for db access
#$config['default']['type'] = 'mysql';      // connection type
#$config['default']['host'] = 'localhost';  // db hostname
#$config['default']['name'] = 'address';     // db name
#$config['default']['user'] = 'root';     // db username
#$config['default']['pass'] = 'dev';     // db password
#$config['default']['persistent'] = false;  // db connection persistence?



MYSQL=`which mysql`
EXPECTED_ARGS=1
E_BADARGS=65
if [ $# -ne $EXPECTED_ARGS ]
then
  echo "Usage: $0 <schema.sql>"
  exit $E_BADARGS
fi
Q1="CREATE DATABASE IF NOT EXISTS address;"
SQL="${Q1}"
  
$MYSQL -uroot -pdev -e "$SQL" 
cat $1 | $MYSQL address -uroot -pdev
service mysql restart
#sudo /etc/init.d/mysql restart


echo "check the page http://localhost:8450/adminer-4.2.3.php"
