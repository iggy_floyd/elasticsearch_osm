-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server Version:               5.5.46-0+deb8u1 - (Debian)
-- Server Betriebssystem:        debian-linux-gnu
-- HeidiSQL Version:             9.2.0.4947
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Exportiere Struktur von Tabelle address.address
DROP TABLE IF EXISTS `address`;
CREATE TABLE IF NOT EXISTS `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_id` int(11) NOT NULL DEFAULT '0',
  `country` varchar(30) NOT NULL DEFAULT '',
  `zip` varchar(10) NOT NULL DEFAULT '',
  `city` varchar(200) NOT NULL DEFAULT '',
  `street` varchar(200) NOT NULL DEFAULT '',
  `house_nr` varchar(20) NOT NULL DEFAULT '',
  `status` int(11) DEFAULT '0',
  `last_provider` varchar(100) DEFAULT '',
  `latitude` decimal(10,5) DEFAULT '0.00000',
  `longitude` decimal(10,5) DEFAULT '0.00000',
  `correct_address` varchar(300) DEFAULT '',
  `error` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_address_request` (`request_id`),
  CONSTRAINT `FK_address_request` FOREIGN KEY (`request_id`) REFERENCES `request` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

-- Exportiere Daten aus Tabelle address.address: ~52 rows (ungefähr)
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` (`id`, `request_id`, `country`, `zip`, `city`, `street`, `house_nr`, `status`, `last_provider`, `latitude`, `longitude`, `correct_address`, `error`) VALUES
	(4, 17, '', 'sdfsdf', 'dsfsdffd', 'dsfdsf', 'dsfsdf', 0, '', 0.00000, 0.00000, '', 0),
	(5, 18, '', 'sdfsdf', 'dsfsdffd', 'dsfdsf', 'dsfsdf', 1, NULL, NULL, NULL, NULL, 0),
	(6, 19, '', 'sdfsdf', 'dsfsdffd', 'dsfdsf', 'dsfsdf', 1, NULL, NULL, NULL, NULL, 0),
	(7, 20, '', '09648', 'Mittweida', 'Lauenhainer Str', '23', 1, NULL, NULL, NULL, NULL, 0),
	(8, 21, '', '09648', 'Mittweida', 'Lauenhainer Str', '23', 1, NULL, NULL, NULL, NULL, 0),
	(9, 22, '', '09648', 'Mittweida', 'Lauenhainer', '61', 1, NULL, NULL, NULL, NULL, 0),
	(10, 23, '', '09648', 'Mittweida', 'Lauenhainer', '61', 0, 'YQLTable', 50.99301, 12.97169, 'Lauenhainer Straße 61 09648 Mittweida Germany', 0),
	(11, 24, '', '09648', 'Mittweida', 'Luther', '23', 0, 'YQLTable', 50.99244, 12.96938, 'Lutherstraße 23 09648 Mittweida Germany', 0),
	(12, 25, '', '09648', 'Mittweida', 'Rohlitzer', '23', 1, NULL, NULL, NULL, NULL, 0),
	(13, 26, '', '09648', 'Mittweida', 'Rohlitzer', '23', 0, 'YQLTable', 50.98614, 12.97847, 'Rochlitzer Straße 23 09648 Mittweida Germany', 0),
	(14, 27, '', '09648', 'Mittweida', 'Bahnhof', '23', 0, 'OpenMapQuest', 50.98530, 12.96909, '', 0),
	(15, 28, '', '09648', 'Mittweida', 'Bahnhofstr', '2', 0, 'OpenMapQuest', 50.96752, 12.96082, '', 0),
	(16, 29, '', '09648', 'Mittweida', 'steinweg', '2', 0, 'YQLTable', 50.98497, 12.98561, 'Steinweg 2 09648 Mittweida Germany', 0),
	(17, 30, '', '09648', 'Mittweida', 'Bahnhostr', '2', 1, NULL, NULL, NULL, NULL, 0),
	(18, 31, '', '09648', 'Mittweida', 'Bahnhostr', '2', 0, 'YQLTable', 50.98604, 12.97228, 'Bahnhofstraße 2 09648 Mittweida Germany', 0),
	(19, 32, '', '09648', 'Mittweida', 'Bahnhofstr', '2', 0, 'OpenMapQuest', 50.96752, 12.96082, '', 0),
	(20, 33, '', '09648', 'Mittweida', 'bahnhofstraße ', '2', 0, 'OpenMapQuest', 50.96752, 12.96082, '', 0),
	(21, 34, '', '09648', 'Mittweida', 'bahnhofstraße', '1', 0, 'OpenMapQuest', 50.96795, 12.96041, '', 0),
	(22, 35, '', '09648', 'Mittweida', 'bahnhofstraße', '1', 0, 'OpenMapQuest', 50.96795, 12.96041, '', 0),
	(23, 36, '', '09648', 'Mittweida', 'bahnhofstraße', '1', 0, 'OpenMapQuest', 50.96795, 12.96041, '', 0),
	(24, 37, '', '09648', 'Mittweida', 'bahnhofstraße', '1', 0, 'OpenMapQuest', 50.96795, 12.96041, '', 0),
	(25, 38, '', '09648', 'Mittweida', 'bahnhofstraße', '1', 0, 'OpenMapQuest', 50.96795, 12.96041, '', 0),
	(26, 39, '', '09648', 'Mittweida', 'Lauenhainer', '1', 0, 'OpenMapQuest', 50.98822, 12.98010, '', 0),
	(27, 40, '', '09648', 'Mittweida', 'Lauenhainer', '22', 0, 'YQLTable', 50.99137, 12.97500, 'Lauenhainer Straße 22 09648 Mittweida Germany', 0),
	(28, 41, '', '09648', 'Mittweida', 'Lauenhainer Str', '22', 0, 'YQLTable', 50.99137, 12.97500, 'Lauenhainer Straße 22 09648 Mittweida Germany', 0),
	(29, 42, '', '09648', 'Mittweida', 'Lauenhainer Straße', '22', 0, 'YQLTable', 50.99137, 12.97500, 'Lauenhainer Straße 22 09648 Mittweida Germany', 0),
	(30, 43, '', '09648', 'Mittweida', 'Lauenhainer', '22', 0, 'YQLTable', 50.99137, 12.97500, 'Lauenhainer Straße 22 09648 Mittweida Germany', 0),
	(31, 44, '', '09648', 'Mittweida', 'Lauenhainer', '22', 0, 'YQLTable', 50.99137, 12.97500, 'Lauenhainer Straße 22 09648 Mittweida Germany', 0),
	(32, 45, '', '09648', 'Mittweida', 'Lauenhainer', '22', 0, 'YQLTable', 50.99137, 12.97500, 'Lauenhainer Straße 22 09648 Mittweida Germany', 0),
	(33, 46, '', '09648', 'Mittweida', 'Lauenhainer', '22', 0, 'YQLTable', 50.99137, 12.97500, 'Lauenhainer Straße 22 09648 Mittweida Germany', 0),
	(34, 47, '', '09648', 'Mittweida', 'Lauenhainer', '22', 0, 'YQLTable', 50.99137, 12.97500, 'Lauenhainer Straße 22 09648 Mittweida Germany', 0),
	(35, 48, '', '09648', 'Mittweida', 'Lauenhainer', '22', 0, 'YQLTable', 50.99137, 12.97500, 'Lauenhainer Straße 22 09648 Mittweida Germany', 0),
	(36, 49, '', '09648', 'Mittweida', 'Lauenhainer', '22', 0, 'YQLTable', 50.99137, 12.97500, 'Lauenhainer Straße 22 09648 Mittweida Germany', 0),
	(37, 50, '', '09648', 'Mittweida', 'Luther', '22', 1, NULL, NULL, NULL, NULL, 0),
	(38, 51, '', '09648', 'Mittweida', 'Luther', '22', 1, NULL, NULL, NULL, NULL, 0),
	(39, 52, '', '09648', 'Mittweida', 'Luther', '22', 1, NULL, NULL, NULL, '', 0),
	(40, 53, '', '09648', 'Mittweida', 'Luther', '22', 1, NULL, NULL, NULL, '', 0),
	(41, 54, '', '09648', 'Mittweida', 'Luther', '22', 1, NULL, NULL, NULL, '', 0),
	(42, 55, '', '09648', 'Mittweida', 'Luther', '22', 1, NULL, NULL, NULL, '', 0),
	(43, 56, '', '09648', 'Mittweida', 'Luther str', '23', 1, NULL, NULL, NULL, '', 0),
	(44, 57, '', '09648', 'Mittweida', 'Luther str', '23', 0, 'YQLTable', 50.99244, 12.96938, 'Lutherstraße 23 09648 Mittweida Germany', 0),
	(45, 58, '', '09648', 'Mittweida', 'Luther', '23', 0, 'YQLTable', 50.99244, 12.96938, 'Lutherstraße 23 09648 Mittweida Germany', 0),
	(46, 59, '', '09648', 'Mittweida', 'bahnhofstraße', '23', 0, 'OpenMapQuest', 50.98530, 12.96909, '', 0),
	(47, 60, '', '09648', 'Mittweida', 'Lauenhainer', '62', 0, '', 0.00000, 0.00000, '', 0),
	(48, 61, '', '09648', 'Mittweida', 'Lauenhainer', '23', 0, '', 0.00000, 0.00000, '', 0),
	(49, 62, '', '09648', 'Mittweida', 'Lauenhainer', '23', 0, '', 0.00000, 0.00000, '', 0),
	(50, 63, '', '09648', 'Mittweida', 'Lauenhainer', '23', 0, '', 0.00000, 0.00000, '', 0),
	(51, 64, '', '09648', 'Mittweida', 'Lauenhainer', '23', 0, '', 0.00000, 0.00000, '', 0),
	(52, 65, '', '09648', 'Mittweida', 'Lauenhainer', '23', 0, '', 0.00000, 0.00000, '', 0),
	(53, 66, '', '09648', 'Mittweida', 'Lauenhainer', '23', 0, 'Google', NULL, NULL, '', 0),
	(54, 67, '', '09648', 'Mittweida', 'Lauenhainerss', '23', 0, 'Google', NULL, NULL, '', 0),
	(55, 68, '', '09116', 'Chemnitz', 'Gustav-Adolf-Str', '34', 0, 'Osm', 50.83188, 12.88970, '', 0);
/*!40000 ALTER TABLE `address` ENABLE KEYS */;


-- Exportiere Struktur von Tabelle address.request
DROP TABLE IF EXISTS `request`;
CREATE TABLE IF NOT EXISTS `request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT '0',
  `count` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_request_user` (`user_id`),
  CONSTRAINT `FK_request_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;

-- Exportiere Daten aus Tabelle address.request: ~52 rows (ungefähr)
/*!40000 ALTER TABLE `request` DISABLE KEYS */;
INSERT INTO `request` (`id`, `user_id`, `count`, `status`, `create_date`) VALUES
	(17, 1, 1, 0, '2015-12-30 19:09:03'),
	(18, 1, 1, 1, '2015-12-30 19:12:08'),
	(19, 1, 1, 0, '2015-12-30 19:12:22'),
	(20, 1, 1, 0, '2015-12-30 19:14:10'),
	(21, 1, 1, 1, '2015-12-30 19:14:39'),
	(22, 1, 1, 0, '2015-12-30 19:17:03'),
	(23, 1, 1, 0, '2015-12-30 19:19:07'),
	(24, 1, 1, 0, '2015-12-30 19:19:50'),
	(25, 1, 1, 1, '2015-12-30 19:21:09'),
	(26, 1, 1, 0, '2015-12-30 19:21:44'),
	(27, 1, 1, 1, '2015-12-30 19:22:38'),
	(28, 1, 1, 0, '2015-12-30 19:23:10'),
	(29, 1, 1, 0, '2015-12-30 19:24:04'),
	(30, 1, 0, 0, '2015-12-30 20:50:01'),
	(31, 1, 0, 0, '2015-12-30 20:50:48'),
	(32, 1, 0, 0, '2015-12-30 20:51:14'),
	(33, 1, 0, 0, '2015-12-30 20:52:02'),
	(34, 1, 0, 0, '2015-12-30 20:52:28'),
	(35, 1, 0, 0, '2015-12-30 20:53:50'),
	(36, 1, 0, 0, '2015-12-30 20:54:18'),
	(37, 1, 1, 0, '2015-12-30 20:55:00'),
	(38, 1, 1, 0, '2015-12-30 20:55:13'),
	(39, 1, 1, 0, '2015-12-30 20:55:41'),
	(40, 1, 1, 0, '2015-12-30 20:56:36'),
	(41, 1, 1, 0, '2015-12-30 20:57:03'),
	(42, 1, 1, 0, '2015-12-30 20:57:26'),
	(43, 1, 1, 0, '2015-12-30 21:00:45'),
	(44, 1, 1, 0, '2015-12-30 21:01:25'),
	(45, 1, 1, 0, '2015-12-30 21:02:31'),
	(46, 1, 1, 0, '2015-12-30 21:03:30'),
	(47, 1, 1, 0, '2015-12-30 21:03:55'),
	(48, 1, 1, 0, '2015-12-30 21:05:12'),
	(49, 1, 1, 0, '2015-12-30 21:06:10'),
	(50, 1, 1, 1, '2015-12-30 21:17:07'),
	(51, 1, 1, 1, '2015-12-30 21:18:28'),
	(52, 1, 1, 1, '2015-12-30 21:19:41'),
	(53, 1, 1, 1, '2015-12-30 21:20:35'),
	(54, 1, 1, 1, '2015-12-30 21:21:37'),
	(55, 1, 1, 1, '2015-12-30 21:22:08'),
	(56, 1, 1, 1, '2015-12-30 21:22:21'),
	(57, 1, 1, 0, '2015-12-30 21:22:40'),
	(58, 1, 1, 0, '2015-12-30 21:23:27'),
	(59, 1, 1, 0, '2015-12-30 21:23:42'),
	(60, 1, 0, 0, '2016-01-21 20:15:00'),
	(61, 1, 0, 0, '2016-01-21 20:19:23'),
	(62, 1, 0, 0, '2016-01-21 20:19:55'),
	(63, 1, 0, 0, '2016-01-21 20:21:33'),
	(64, 1, 0, 0, '2016-01-21 20:24:56'),
	(65, 1, 0, 0, '2016-01-21 20:28:55'),
	(66, 1, 1, 0, '2016-01-21 20:29:31'),
	(67, 1, 1, 0, '2016-01-21 20:30:15'),
	(68, 1, 1, 0, '2016-01-21 20:31:46');
/*!40000 ALTER TABLE `request` ENABLE KEYS */;


-- Exportiere Struktur von Tabelle address.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(100) NOT NULL DEFAULT '0',
  `password` varchar(100) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Exportiere Daten aus Tabelle address.user: ~1 rows (ungefähr)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `user`, `password`) VALUES
	(1, 'test', 'test');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
