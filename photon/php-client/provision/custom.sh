#!/bin/bash

echo "Installing additional dependent packages"
sudo apt-get update

mkdir -p src/ 2>/dev/null


# Installing basic software: wget  git-core ca-certificates
sudo apt-get -y install sudo wget git-core ca-certificates
#sudo apt-get -y install build-essential libxml2-dev libbz2-dev libtool automake 
#sudo apt-get -y install libboost-dev libboost-system-dev libboost-filesystem-dev libboost-thread-dev libexpat-dev
#sudo apt-get -y install zlib1g-dev libncurses5-dev libreadline6-dev libexpat1-dev libdb-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev
#sudo apt-get -y install gcc proj-bin libgeos-c1 libgeos++-dev  build-essential 


sudo apt-get -y install pkg-config

 
# install apache2 and PHP5
sudo apt-get -y install apache2
sudo apt-get -y install php5 libapache2-mod-php5
sudo apt-get -y install php5-common php5-cli  php5-cgi php5-mysql php5-curl php5-gd php5-intl php-pear php5-imagick php5-imap php5-mcrypt php5-memcache php5-pspell php5-recode php5-snmp php5-sqlite php5-tidy php5-xmlrpc php5-xsl php5-apcu php5-mcrypt
sudo apt-get -y install curl libcurl3 libcurl3-dev

mkdir -p /home/vagrant/project/www/address.validator.com/public
mkdir -p /home/vagrant/project/www/address.validator.com/logs/
sudo /etc/init.d/apache2 restart

# add a new VirtualHost
cat <<_END >address.validator.com.conf
<VirtualHost *:80> 
     ServerAdmin webmaster@address.validator.com
     ServerName  address.validator.com
     ServerAlias www.address.validator.com
     DocumentRoot /home/vagrant/project/www/address.validator.com/public
     ErrorLog /home/vagrant/project/www/address.validator.com/logs/error.log 
     CustomLog /home/vagrant/project/www/address.validator.com/logs/access.log combined
</VirtualHost>

_END

sudo mv address.validator.com.conf  /etc/apache2/sites-available/address.validator.com.conf

# add info.php to show available configuration
cat <<_END >> info.php
<?php
phpinfo();
?>
_END

mv info.php  /home/vagrant/project/www/address.validator.com/public



# enable the new VirtualHost
sudo a2ensite address.validator.com.conf
sudo /etc/init.d/apache2 restart

# fix the default DHost to the new added one
sudo mv /etc/apache2/sites-enabled/000-default.conf /etc/apache2/sites-enabled/100-default.conf
sudo mv /etc/apache2/sites-enabled/address.validator.com.conf /etc/apache2/sites-enabled/000-address.validator.com.conf
sudo /etc/init.d/apache2 restart


# add access to the VirtualHost

sudo sh -c 'echo  " <Directory /home/vagrant/project/www/address.validator.com/public>" >> /etc/apache2/apache2.conf'
sudo sh -c 'echo  "     Options FollowSymLinks " >> /etc/apache2/apache2.conf '
sudo sh -c 'echo  "     AllowOverride All " >> /etc/apache2/apache2.conf'
sudo sh -c 'echo  "     Require all granted " >> /etc/apache2/apache2.conf'
sudo sh -c 'echo  " </Directory>" >> /etc/apache2/apache2.conf'

sudo /etc/init.d/apache2 restart


# enable rewrite mode
sudo a2enmod rewrite
sudo a2enmod headers
sudo /etc/init.d/apache2 restart

sudo sh -c 'echo  " source /home/vagrant/project/virtualenvs/address-validation/bin/activate " >> /home/vagrant/.bashrc'

# add mysql
sudo ${1}provision/install_my_sql.sh  ${1}provision/000001.sql;


# fix a problem with nc
sudo apt-get install netcat -y
rm /bin/nc
ln -s /bin/nc.traditional /bin/nc


echo "
	please, visit

        http://localhost:8430
or	

	http://localhost:8430/info.php

to be sure that you have installed LAMP server properly!

do disable the site, please do

	sudo a2dissite address.validator.com.conf

"
