#!/bin/bash

/etc/init.d/ssh start

. /app/virtualenvs/photon/bin/activate

# if VOLUME
# changing layout of the web site
mv /app.py /app/website/photon/app.py
mv /index.html /app/website/photon/templates/index.html
mv /ngnix_forward_proxy /app/website/ngnix_forward_proxy

eval $'cat <<\002\n'"$(</app/website/ngnix_forward_proxy)"$'\n\002' > /etc/nginx/sites-enabled/default
service nginx restart

# if no VOLUME
#python photon/app.py

# if VOLUME
python  /app/website/photon/app.py
