/**
 * Created by i.marfin on 1/11/16.
 */
angular.module('mainApp', [])
    .config(['$interpolateProvider', function($interpolateProvider) {
        $interpolateProvider.startSymbol('{[');
        $interpolateProvider.endSymbol(']}');
    }])
    .factory('MathService', function() {
        var factory = {};

        factory.multiply = function(a, b) {
            return a * b
        }
        return factory;
    })
    .service('CalcService', function(MathService){
        this.square = function(a) {
            return MathService.multiply(a,a);
        }
    })
    .controller('CalcController', function($scope, CalcService) {
        $scope.square = function() {
            $scope.result = CalcService.square($scope.number);
        }
    });

