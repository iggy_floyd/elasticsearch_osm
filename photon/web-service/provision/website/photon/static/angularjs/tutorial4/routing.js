/**
 * Created by i.marfin on 1/11/16.
 */
angular.module('mainApp', ['ngRoute'])
    .config(['$interpolateProvider', function($interpolateProvider) {
        $interpolateProvider.startSymbol('{[');
        $interpolateProvider.endSymbol(']}');
    }])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.

            when('/addStudent', {
                templateUrl: '/static/angularjs/tutorial4/views/addstudent.tpl',
                controller: 'AddStudentController'
            }).

            when('/viewStudents', {
                templateUrl: '/static/angularjs/tutorial4/views/viewstudent.tpl',
//                templateUrl:   'viewStudents.htm',
                controller: 'ViewStudentsController'
            }).
            otherwise({
                redirectTo: '/addStudent'
            });
    }])
    .controller('AddStudentController', function($scope) {
        $scope.message = "This page will be used to display add student form";
    })
    .controller('ViewStudentsController', function($scope) {
    $scope.message = "This page will be used to display all the students";
    });

