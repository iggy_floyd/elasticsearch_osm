/**
 * Created by i.marfin on 1/11/16.
 */
angular.module('mainApp', ['ngRoute','ngTable'])
    .config(['$interpolateProvider', function($interpolateProvider) {
        $interpolateProvider.startSymbol('{[');
        $interpolateProvider.endSymbol(']}');
    }])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider
            // route for the home page
            .when('/', {
                templateUrl : '/static/angularjs/tutorial8/views/home.tpl',
                controller  : 'mainController'
            })
            // route for the about page
            .when('/about', {
                templateUrl : 'static/angularjs/tutorial8/views/about.tpl',
                controller  : 'aboutController'
            })

            // route for the contact page
            .when('/contact', {
                templateUrl : 'static/angularjs/tutorial8/views/contact.tpl',
                controller  : 'contactController'
            });

    }])
    .controller('mainController', function($scope, $filter, ngTableParams) {

        $scope.items = [
            {"id":"1","name":"name 1","description":"description 1","field3":"field3 1","field4":"field4 1","field5":"field5 1"},
            {"id":"2","name":"name 2","description":"description 2","field3":"field3 2","field4":"field4 2","field5":"field5 2"},
            {"id":"3","name":"name 3","description":"description 3","field3":"field3 3","field4":"field4 3","field5":"field5 3"},
            {"id":"4","name":"name 4","description":"description 4","field3":"field3 4","field4":"field4 4","field5":"field5 4"},
            {"id":"5","name":"name 5","description":"description 5","field3":"field3 5","field4":"field4 5","field5":"field5 5"},
            {"id":"6","name":"name 6","description":"description 6","field3":"field3 6","field4":"field4 6","field5":"field5 6"},
            {"id":"7","name":"name 7","description":"description 7","field3":"field3 7","field4":"field4 7","field5":"field5 7"},
            {"id":"8","name":"name 8","description":"description 8","field3":"field3 8","field4":"field4 8","field5":"field5 8"},
            {"id":"9","name":"name 9","description":"description 9","field3":"field3 9","field4":"field4 9","field5":"field5 9"},
            {"id":"10","name":"name 10","description":"description 10","field3":"field3 10","field4":"field4 10","field5":"field5 10"}
        ];
        var a=$scope.items.slice(1,2)
        console.log("a");

        $scope.usersTable = new ngTableParams({
            page: 4,
            count: 3
        }, {
            total: $scope.items.length,
            getData: function ($defer, params) {
                $scope.data = params.sorting() ? $filter('orderBy')($scope.items, params.orderBy()) : $scope.items;
                $scope.data = params.filter() ? $filter('filter')($scope.data, params.filter()) : $scope.data;
                $scope.data = $scope.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                console.log($scope.data);
                $defer.resolve($scope.data);
            }
        });


    })
    .controller('aboutController', function($scope) {
        $scope.message = 'This is an example of the ng-tables';
    })
    .controller('contactController', function($scope) {
        $scope.message = 'Contact me: iggy.floyd.de@gmail.com';
    });