<div class="jumbotron text-center">
    <h1>Home Page</h1>
    <br>
    <br>

    <table ng-table="usersTable" show-filter="true" class="table table-striped">
        <tr ng-repeat="user in data">
            <td data-title="'Id'"  sortable="'id'" >
                {[user.id]}
            </td>
            <td data-title="'First Name'"  sortable="'name'" filter="{ 'name': 'text' }" >
                {[user.name]}
            </td>
            <td data-title="'Description'">
                {[user.description]}
            </td>

        </tr>
    </table>

</div>