<div class="jumbotron text-center">
    <h1>Home Page</h1>
    <br>
    <br>


    <h2>Provide me Address</h2>
    <label for="MyField3">Comma-separated address: street,houseNr,postcode,city,country
    <input type="text" ng-model="address "  id="MyField3" placeholder="Enter an address"  />
    </label>
    <label for="MyField">Limit
    <input type="text" ng-model="limit "  id='MyField' placeholder="Enter a limit"  />
    </label>
    <label for="MyField2">Language(en|de|fr|it)
        <input type="text" ng-model="lang "  id='MyField2' placeholder="Enter a language"  />
    </label>
    <br>
    <br>
    <div ng-validator ng-model="address" ng-limit="limit" ng-lang="lang"></div>
    <br>
    <br>

    <table ng-table="usersTable" show-filter="true" class="table table-striped">
        <tr ng-repeat="item in data">
            <td data-title="'Name'" sortable="'name'" filter="{ 'name': 'text' }"  >
                {[item.name | nocmdline ]}
            </td>
            <td data-title="'Type'" sortable="'osm_key'" filter="{ 'osm_key': 'text' }"  >
                {[item.osm_key]}
            </td>
            <td data-title="'Street'" sortable="'street'" filter="{ 'street': 'text' }" >
                {[item.street]}
            </td>
            <td data-title="'HouseNr'" sortable="'housenumber'" filter="{ 'housenumber': 'text' }" >
                {[item.housenumber]}
            </td>
            <td data-title="'Postcode'" sortable="'postcode'" filter="{ 'postcode': 'text' }" >
                {[item.postcode]}
            </td>
            <td data-title="'City'"  sortable="'city'" filter="{ 'city': 'text' }">
                {[item.city]}
            </td>
            <td data-title="'Country'"  sortable="'country'" filter="{ 'country': 'text' }">
                {[item.country]}
            </td>
            <td data-title="'Validation_status'"  sortable="'validation_status'" filter="{ 'validation_status': 'text' }" >
                {[item.validation_status]}
            </td>
            <td data-title="'Similarity'"  sortable="'similarity'"   filter="{ 'similarity':  'text' }" >
                {[item.similarity ]}
            <!--
                {[item.similarity | greater : 0.1  ]}
            -->
            </td>

        </tr>
    </table>



</div>