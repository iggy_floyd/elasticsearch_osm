/**
 * Created by i.marfin on 1/11/16.
 */

var stop_city = function (city) {
    if (!city) return false;
    return city.toLowerCase().indexOf("\\n") !== -1;
};

/*

 "{f} {b}".printf({f: "foo", b: "bar"});

 "%s %s".printf(["foo", "bar"]);

 "%s %s".printf("foo", "bar");

 // all of which give this result:

 "foo bar"

 */

String.prototype.printf = function (obj) {
    var useArguments = false;
    var _arguments = arguments;
    var i = -1;
    if (typeof _arguments[0] == "string") {
        useArguments = true;
    }
    if (obj instanceof Array || useArguments) {
        return this.replace(/\%s/g,
            function (a, b) {
                i++;
                if (useArguments) {
                    if (typeof _arguments[i] == 'string') {
                        return _arguments[i];
                    }
                    else {
                        throw new Error("Arguments element is an invalid type");
                    }
                }
                return obj[i];
            });
    }
    else {
        return this.replace(/{([^{}]*)}/g,
            function (a, b) {
                var r = obj[b];
                return typeof r === 'string' || typeof r === 'number' ? r : a;
            });
    }
};


function levenshtein_distance (a, b) {
    if(a.length == 0) return 0.;
    if(b.length == 0) return 0.;

    var matrix = [];

    // increment along the first column of each row
    var i;
    for(i = 0; i <= b.length; i++){
        matrix[i] = [i];
    }

    // increment each column in the first row
    var j;
    for(j = 0; j <= a.length; j++){
        matrix[0][j] = j;
    }

    // Fill in the rest of the matrix
    for(i = 1; i <= b.length; i++){
        for(j = 1; j <= a.length; j++){
            if(b.charAt(i-1) == a.charAt(j-1)){
                matrix[i][j] = matrix[i-1][j-1];
            } else {
                matrix[i][j] = Math.min(matrix[i-1][j-1] + 1, // substitution
                    Math.min(matrix[i][j-1] + 1, // insertion
                        matrix[i-1][j] + 1)); // deletion
            }
        }
    }

/*    console.log(a);
    console.log(b);
    console.log(matrix[b.length][a.length]);
*/
    return Math.max(0.,1.-matrix[b.length][a.length]/(Math.min(b.length,a.length)));
}

var my_comparator = function(val){
    return function(item){
        return parseFloat(item) > parseFloat(val);
    }
};

Object.defineProperty( Array.prototype,'has',
    {
        value:function(o,flag){
            if(flag === undefined){
                return this.indexOf(o) !== -1;
            }
            else{   // only for raw js object
                for(var v in this){
                    if(JSON.stringify(this[v]) === JSON.stringify(o)) return true;
                }
                return false;
            }
        }
            // writable:false,
            // enumerable:false
    }
);

angular.module('mainApp', ['ngRoute','ngTable','ngSanitize'])
    .config(['$interpolateProvider', function($interpolateProvider) {
        $interpolateProvider.startSymbol('{[');
        $interpolateProvider.endSymbol(']}');
    }])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider
            // route for the home page
            .when('/', {
                templateUrl : '/static/angularjs/tutorial11/views/home.tpl',
                controller  : 'mainController'
            })
            // route for the about page
            .when('/about', {
                templateUrl : 'static/angularjs/tutorial11/views/about.tpl',
                controller  : 'aboutController'
            })

            // route for the contact page
            .when('/contact', {
                templateUrl : 'static/angularjs/tutorial11/views/contact.tpl',
                controller  : 'contactController'
            });

    }])
    // user-defiend filters
    .filter('nocmdline', function () {
        return function (value) {
            return (!value) ? '' : value.replace(/\\n/g, ' ');
        };
    })
    .filter("sanitize", ['$sce', function($sce) {
        return function(htmlCode){
            return $sce.trustAsHtml(htmlCode);
        };
    }])
    .filter("greater", function() {
        return function(item,val){
            return (parseFloat(item) > parseFloat(val))?item:val;
        };
    })

// directives
    // on-read-file: only link function is defined here
    .directive('ngLimit', function() {
    return {
        controller: function($scope) {}
    }
    })
    .directive('ngLang', function() {
        return {
            controller: function($scope) {}
        }
    })
    .directive("ngValidator",['nocmdlineFilter','greaterFilter','ngTableParams',function(nocmdlineFilter,greaterFilter,ngTableParams){
        return {
            restrict:'A',
            require: '^ngModel',
            scope: {
                ngModel: '=',
                ngLimit: '=',
                ngLang: '='
            },
            template: '<div><h1>Check the Address for "{[ngModel  | nocmdline ]}"</h1></div>',

            controller: ['$scope', '$http','$log','$filter', function($scope, $http,$log,$filter) {
                var request='select item from weather.forecast where  woeid in (select woeid from geo.places(1) where text="%s")';
//                var apiurl ='http://localhost:8553/api/?q=';
                var apiurl ='http://localhost:5001/api/?q=';

//                $log.log($scope.ngLimit);

                $scope.items = [ ]
                $scope.getData = function(address,limit,lang) {
                    if (!address) return;
                    if (!limit) limit = 200;
                    if (!lang)  lang = "de";
                    if (!['de','it','fr','en'].has(lang))  return;
                    address = address.replace(/\,/g, ' ');
                    $log.log(address);
                    // get data
//                    console.log( apiurl + encodeURIComponent(address)+"&limit="+limit+"&lang="+lang);
                    $http({
                        method: 'GET',
                        url: apiurl + encodeURIComponent(address)+"&limit="+limit+"&lang="+lang
                    }).then(
                        function successCallback(response) {
                            $scope.items = [ ]
                            $scope.$parent.data = [];
                            // if we got addresses....
//                            $log.log(JSON.stringify(response)+"\n\n");
                            var data=response.data.features;
                            angular.forEach(data,function(value,key){
//                                    $log.log(key+"\n\n");
//                                    $log.log(value);

                                    var item = {
                                        "name":value.properties.name || "NOT_VALID",
                                        "osm_key":value.properties.osm_key || "NOT_VALID",
                                        "street":value.properties.street || "NOT_VALID",
                                        "housenumber":value.properties.housenumber || "NOT_VALID",
                                        "postcode":value.properties.postcode || "NOT_VALID",
                                        "city":value.properties.city || "NOT_VALID",
                                        "country":value.properties.country || "NOT_VALID",
                                        "validation_status":    (value.properties.street &&
                                            value.properties.housenumber &&
                                            value.properties.postcode &&
                                            value.properties.city &&
                                            value.properties.country) ? 'VALID' : "NOT_VALID"
                                    };
//                                $log.log(address.toLowerCase());
//                                $log.log((item['street']+','+item['housenumber']+' '+item['postcode']+' '+item['city']+' '+item['country']).toLowerCase());
//                                $log.log(levenshtein_distance(address.toLowerCase(),(item['street']+' '+item['housenumber']+' '+item['postcode']+' '+item['city']+' '+item['country']).toLowerCase()));
//                                item.similarity = levenshtein_distance(address.toLowerCase(),(item['street']+','+item['housenumber']+','+item['postcode']+','+item['city']+','+item['country']).toLowerCase());
                                item.similarity = levenshtein_distance(address.toLowerCase(),(item['street']+' '+item['housenumber']+' '+item['postcode']+' '+item['city']+' '+item['country']).toLowerCase());
                                $scope.items.push(item);
                                });
                            if ($scope.items.length == 0 ) {
                                $scope.items.push({
                                    "name": "NOT_VALID",
                                    "osm_key":"NOT_VALID",
                                    "street": "NOT_VALID",
                                    "housenumber": "NOT_VALID",
                                    "postcode": "NOT_VALID",
                                    "city": "NOT_VALID",
                                    "country": "NOT_VALID",
                                    "validation_status":"NOT_VALID",
                                    "similarity":0.
                                    });
                            }
                            $scope.$parent.usersTable = new ngTableParams({
                                page: 1,
                                count: 10,
                                filter: {
                                    similarity:greaterFilter(0.2)
                                }
                            }, {
                                total: $scope.items.length,
                                getData: function ($defer, params) {
/*                                    if ( params.filter() ) {
                                        console.log(params.filter());
                                        console.log(params.filter().similarity? my_comparator:undefined);
                                    }
*/
                                    $scope.$parent.data = params.sorting() ? $filter('orderBy')($scope.items, params.orderBy()) : $scope.items;
//                                    var filterInfo = params.filter();
//                                    var comparer = (filterInfo && filterInfo['similarity']) ? my_comparator : undefined;
//                                    console.log(comparer);
//                                    console.log(filterInfo);
//                                    $log.log(params);

                                    //$scope.$parent.data = filterInfo ?  $filter('filter')($scope.$parent.data,comparer): $scope.$parent.data;
                                    $scope.$parent.data = params.filter() ?  $filter('filter')($scope.$parent.data,params.filter()): $scope.$parent.data;
                                    //$scope.$parent.data = params.filter() ? $filter('filter')($scope.$parent.data, params.filter(),params.filter().similarity? my_comparator:undefined ) : $scope.$parent.data;
                                    $scope.$parent.data = $scope.$parent.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
//                                    console.log($scope.$parent.data);
                                    $defer.resolve($scope.$parent.data);
                                }
                            });
                            $scope.$parent.usersTable.reload();

                        },function(error) {
                        alert(JSON.stringify(error));
                    }, function(notify) {
                        alert(JSON.stringify(notify));
                    });
                };

            }],
            link: function(scope, iElement, iAttrs,ngModel,ngLimit,ngLang) {
                // watch changing the input field
                 scope.$watchCollection('[ngModel,ngLimit,ngLang]',
                 function (newValues, oldValues, scope) {
//                 console.log(newValues[0]);
//                 console.log(newValues[1]);
//                 console.log(newValues[2]);
                     setTimeout(scope.getData(nocmdlineFilter(newValues[0]),newValues[1],newValues[2]) );

                 });


/*                scope.$watch('[ngModel,ngLimit]',function (newValues, oldValues, scope) {
                    console.log(newValues[0]);
                    console.log(newValues[1]);
                });
*/
                /*                scope.$watch(function () {
                                    return ngModel.$modelValue;
                                }, function(newValue) {
                                    console.log(newValue);
                                    console.log(ngLimit);
                                    setTimeout(scope.getData(nocmdlineFilter(newValue)), 300);
                //                    if(stop_city(newValue)) {
                //                        console.log(nocmdlineFilter(newValue));
                //                        scope.getData(nocmdlineFilter(newValue));
                //                    }

                                });
                */

            }
        }
    }])

    // controllers
    .controller('mainController', function($scope, $filter, ngTableParams) {

        $scope.filter = {
            similarity:my_comparator(0.20)
        };


    })
    .controller('aboutController', function($scope) {
        $scope.message = 'This is an example of ElasticOSM Client';
    })
    .controller('contactController', function($scope) {
        $scope.message = 'Contact me: iggy.floyd.de@gmail.com';
    });

