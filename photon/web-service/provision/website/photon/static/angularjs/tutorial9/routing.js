/**
 * Created by i.marfin on 1/11/16.
 */
angular.module('mainApp', ['ngRoute','ngTable'])
    .config(['$interpolateProvider', function($interpolateProvider) {
        $interpolateProvider.startSymbol('{[');
        $interpolateProvider.endSymbol(']}');
    }])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider
            // route for the home page
            .when('/', {
                templateUrl : '/static/angularjs/tutorial9/views/home.tpl',
                controller  : 'mainController'
            })
            // route for the about page
            .when('/about', {
                templateUrl : 'static/angularjs/tutorial9/views/about.tpl',
                controller  : 'aboutController'
            })

            // route for the contact page
            .when('/contact', {
                templateUrl : 'static/angularjs/tutorial9/views/contact.tpl',
                controller  : 'contactController'
            });

    }])
    // directives
    // on-read-file: only link function is defined here
    .directive("onReadFile",function($parse,$log){
        return {
            restrict:'A',
            scope:false, // we will use the parent scope
            link: function(scope,element,attrs) {
                var fn =  $parse(attrs.onReadFile);
                element.on('change', function(onChangeEvent) {
                    // create FileReader;
                    var reader = new FileReader();
                    reader.onload = function(onLoadEvent) {
                            // $apply acts asynchronously!
                            scope.$apply(function() {
                            console.log(onLoadEvent.target.result);
                            fn(scope, {$fileContent:onLoadEvent.target.result});
                        });
                    };
                    console.log((onChangeEvent.srcElement || onChangeEvent.target).files);
                    reader.readAsText((onChangeEvent.srcElement || onChangeEvent.target).files[0]);
                });
            }
        }
    })

    // controllers
    .controller('mainController', function($scope, $filter, ngTableParams) {

        // showContent function
        $scope.showContent = function($fileContent){
            $scope.items = JSON.parse($fileContent);
            $scope.content = $fileContent;
            $scope.usersTable.total = $scope.items.length;
            $scope.usersTable = new ngTableParams({
                page: 1,
                count: 5
            }, {
                total: $scope.items.length,
                getData: function ($defer, params) {
                    $scope.data = params.sorting() ? $filter('orderBy')($scope.items, params.orderBy()) : $scope.items;
                    $scope.data = params.filter() ? $filter('filter')($scope.data, params.filter()) : $scope.data;
                    $scope.data = $scope.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                    console.log($scope.data);
                    $defer.resolve($scope.data);
                }
            });
            $scope.usersTable.reload();
        };
        $scope.items = []

        $scope.usersTable = new ngTableParams({
            page: 1,
            count: 5
        }, {
            total: $scope.items.length,
            getData: function ($defer, params) {
                $scope.data = params.sorting() ? $filter('orderBy')($scope.items, params.orderBy()) : $scope.items;
                $scope.data = params.filter() ? $filter('filter')($scope.data, params.filter()) : $scope.data;
                $scope.data = $scope.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                console.log($scope.data);
                $defer.resolve($scope.data);
            }
        });


        $scope.message = 'This is an example of the ng-table with reading json files';

    })
    .controller('aboutController', function($scope) {
        $scope.message = 'This is an example of the the ng-table with reading json files in AngularJS';
    })
    .controller('contactController', function($scope) {
        $scope.message = 'Contact me: iggy.floyd.de@gmail.com';
    });