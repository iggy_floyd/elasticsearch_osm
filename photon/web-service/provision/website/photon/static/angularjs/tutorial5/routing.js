/**
 * Created by i.marfin on 1/11/16.
 */
angular.module('mainApp', ['ngRoute'])
    .config(['$interpolateProvider', function($interpolateProvider) {
        $interpolateProvider.startSymbol('{[');
        $interpolateProvider.endSymbol(']}');
    }])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider
            // route for the home page
            .when('/', {
                templateUrl : '/static/angularjs/tutorial5/views/home.tpl',
                controller  : 'mainController'
            })
            // route for the about page
            .when('/about', {
                templateUrl : 'static/angularjs/tutorial5/views/about.tpl',
                controller  : 'aboutController'
            })

            // route for the contact page
            .when('/contact', {
                templateUrl : 'static/angularjs/tutorial5/views/contact.tpl',
                controller  : 'contactController'
            });

    }])
    .controller('mainController', function($scope) {
        // create a message to display in our view
        $scope.message = 'Everyone come and see how good I look!';
    })
    .controller('aboutController', function($scope) {
        $scope.message = 'Look! I am an about page.';
    })
    .controller('contactController', function($scope) {
        $scope.message = 'Contact us! JK. This is just a demo.';
    });