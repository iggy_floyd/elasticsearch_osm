<div class="jumbotron text-center">
    <h1>Home Page</h1>
    <table border = "0">
        <tr>
            <td>Enter the first name of the Student:</td>
            <td><input type = "text" ng-model = "student.firstName"></td>
        </tr>

        <tr>
            <td>Enter the last name of the Student: </td>
            <td><input type = "text" ng-model = "student.lastName"></td>
        </tr>

        <tr>
            <td>Enter student's fees: </td>
            <td><input type = "text" ng-model = "student.fees"></td>
        </tr>

        <tr>
            <td>Enter subject of interests: </td>
            <td><input type = "text" ng-model = "subjectName"></td>
        </tr>
    </table>

    <!--
            Here is an example of the use filters: uppercase, lowercase, filter:<property> and orderBy
    -->
    <br>
    <br>
    <br>


    <table border = "1">
        <tr>
            <td>Name in Upper Case: </td><td>{[student.fullName() | uppercase]}</td>
        </tr>

        <tr>
            <td>Name in Lower Case: </td><td>{[student.fullName() | lowercase]}</td>
        </tr>

        <tr>
            <td>fees: </td><td>{[student.fees | currency]}
            </td>
        </tr>

        <tr>
            <td>Subject:</td>

            <td>
                <ul>
                    <li ng-repeat = "subject in student.subjects | filter: subjectName |orderBy:'marks'">
                        {[ subject.name + ', marks:' + subject.marks ]}
                    </li>
                </ul>
            </td>
        </tr>
    </table>


</div>