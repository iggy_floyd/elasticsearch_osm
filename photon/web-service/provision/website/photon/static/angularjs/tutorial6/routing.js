/**
 * Created by i.marfin on 1/11/16.
 */
angular.module('mainApp', ['ngRoute'])
    .config(['$interpolateProvider', function($interpolateProvider) {
        $interpolateProvider.startSymbol('{[');
        $interpolateProvider.endSymbol(']}');
    }])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider
            // route for the home page
            .when('/', {
                templateUrl : '/static/angularjs/tutorial6/views/home.tpl',
                controller  : 'mainController'
            })
            // route for the about page
            .when('/about', {
                templateUrl : 'static/angularjs/tutorial6/views/about.tpl',
                controller  : 'aboutController'
            })

            // route for the contact page
            .when('/contact', {
                templateUrl : 'static/angularjs/tutorial6/views/contact.tpl',
                controller  : 'contactController'
            });

    }])
    .controller('mainController', function($scope) {

        // our student object
        $scope.student = {
            firstName: "Mahesh",
            lastName: "Parashar",
            fees:500,
            subjects:[
                {name:'Physics',marks:70},
                {name:'Chemistry',marks:80},
                {name:'Math',marks:65}
            ],
            fullName: function() {
                var studentObject=this;
                return studentObject.firstName + " " + studentObject.lastName;
            }
        };

    })
    .controller('aboutController', function($scope) {
        $scope.message = 'This is an example of the filtering with AngularJS';
    })
    .controller('contactController', function($scope) {
        $scope.message = 'Contact me: iggy.floyd.de@gmail.com';
    });