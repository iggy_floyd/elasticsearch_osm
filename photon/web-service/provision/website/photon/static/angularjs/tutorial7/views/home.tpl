<div class="jumbotron text-center">
    <h1>Home Page</h1>
    <br>
    <br>
    <pre>pagedItems.length: {[pagedItems.length|json]}</pre>
    <pre>currentPage: {[currentPage|json]}</pre>
    <pre>SortOptions: {[sort|json]}</pre>
    <input type="text" ng-model="query" ng-change="search()" class="input-large search-query"/>
    <table class="table table-striped table-condensed table-hover">
        <thead>
        <tr>
            <th class="id" custom-sort order="'id'" sort="sort"> Id</th>
            <th class="name" custom-sort order="'name'" sort="sort">Name</th>
            <th class="description" custom-sort order="'description'" sort="sort">Description</th>
            <th class="field3" custom-sort order="'field3'" sort="sort">Field 3</th>
            <th class="field4" custom-sort order="'field4'" sort="sort">Field 4</th>
            <th class="field5" custom-sort order="'field5'" sort="sort">Field 5</th>
        </tr>
        </thead>
        <tfoot>
            <td colspan="6">
                <div class="pagination pull-right">
                    <ul>
                        <li ng-class="{disabled: currentPage == 0}">
                            <a href ng-click="prevPage()">« Prev</a>
                        </li>
                        <li ng-repeat="n in range(pagedItems.length, currentPage, currentPage + gap) "
                            ng-class="{active: n == currentPage}"
                            ng-click="setPage()">
                            <a href ng-bind="n + 1">1</a>
                        </li>
                        <li ng-class="{disabled: (currentPage) == pagedItems.length - 1}">
                            <a href ng-click="nextPage()">›</a>
                        </li>
                        <li ng-class="{disabled: (currentPage) == pagedItems.length - 1}" ng-show="pagedItems.length > 5" ng-click="setPage(pagedItems.length-1)">
                            <a href>»</a>
                        </li>
                    </ul>
                </div>
            </td>
        </tfoot>

        <tbody>
        <tr ng-repeat="item in pagedItems[currentPage] | orderBy:sort.sortingOrder:sort.reverse">
            <td>{[item.id]}</td>
            <td>{[item.name]}</td>
            <td>{[item.description]}</td>
            <td>{[item.field3]}</td>
            <td>{[item.field4]}</td>
            <td>{[item.field5]}</td>
        </tr>
        </tbody>
    </table>

</div>