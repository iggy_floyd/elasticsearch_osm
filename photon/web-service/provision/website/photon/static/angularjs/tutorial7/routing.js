/**
 * Created by i.marfin on 1/11/16.
 */
angular.module('mainApp', ['ngRoute'])
    .config(['$interpolateProvider', function($interpolateProvider) {
        $interpolateProvider.startSymbol('{[');
        $interpolateProvider.endSymbol(']}');
    }])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider
            // route for the home page
            .when('/', {
                templateUrl : '/static/angularjs/tutorial7/views/home.tpl',
                controller  : 'mainController'
            })
            // route for the about page
            .when('/about', {
                templateUrl : 'static/angularjs/tutorial7/views/about.tpl',
                controller  : 'aboutController'
            })

            // route for the contact page
            .when('/contact', {
                templateUrl : 'static/angularjs/tutorial7/views/contact.tpl',
                controller  : 'contactController'
            });

    }])
    .controller('mainController', function($scope,$filter) {

        //Sorting Options
        $scope.sort = {
            sortingOrder : 'id',
            reverse : false
        };
        $scope.pagedItems = []; // to be used by pagination
        $scope.currentPage = 0; // the current page
        $scope.groupedItems = [];
        $scope.filteredItems = [];// to be used to store filtered ('searched') items
        $scope.pagedItems = []; // store pages of the items
        $scope.itemsPerPage = 5; // how many items per page do we need?
        $scope.gap = $scope.itemsPerPage; // how many items per page do we need?
        // data in any format!
        $scope.items = [
            {"id":"1","name":"name 1","description":"description 1","field3":"field3 1","field4":"field4 1","field5":"field5 1"},
            {"id":"2","name":"name 2","description":"description 2","field3":"field3 2","field4":"field4 2","field5":"field5 2"},
            {"id":"3","name":"name 3","description":"description 3","field3":"field3 3","field4":"field4 3","field5":"field5 3"},
            {"id":"4","name":"name 4","description":"description 4","field3":"field3 4","field4":"field4 4","field5":"field5 4"},
            {"id":"5","name":"name 5","description":"description 5","field3":"field3 5","field4":"field4 5","field5":"field5 5"},
            {"id":"6","name":"name 6","description":"description 6","field3":"field3 6","field4":"field4 6","field5":"field5 6"},
            {"id":"7","name":"name 7","description":"description 7","field3":"field3 7","field4":"field4 7","field5":"field5 7"},
            {"id":"8","name":"name 8","description":"description 8","field3":"field3 8","field4":"field4 8","field5":"field5 8"},
            {"id":"9","name":"name 9","description":"description 9","field3":"field3 9","field4":"field4 9","field5":"field5 9"},
            {"id":"10","name":"name 10","description":"description 10","field3":"field3 10","field4":"field4 10","field5":"field5 10"}
        ];

        var toType = function(obj) {
            return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
        }
        // to be used by the search query!
        var searchMatch = function (haystack, needle) {
            if (!needle) {
                return true;
            }
//            console.log("haystack>"+toType(haystack));
//            console.log("needle>"+toType(needle));
            return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
        };

        $scope.search = function () {
            $scope.filteredItems = $filter('filter')($scope.items, function (item) {
                for(var attr in item) {
//                    console.log("attr>"+toType(item[attr]));
//                    console.log("query>"+toType($scope.query));
                    if (searchMatch(item[attr], $scope.query)) {
                        return true;
                    }
                }

                return false;
            });
            // take care of the sorting order
            if ($scope.sort.sortingOrder !== '') {
                $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sort.sortingOrder, $scope.sort.reverse);
            }

            console.log( $scope.filteredItems);
            $scope.currentPage = 0;
            // now group by pages
            $scope.groupToPages();
        };

        // calculate page in place
        $scope.groupToPages = function () {
            $scope.pagedItems = [];

            for (var i = 0; i < $scope.filteredItems.length; i++) {
                if (i % $scope.itemsPerPage === 0) {
                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
                } else {
                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
                }
            }
        };

        $scope.range = function (size,start, end) {
            var ret = [];
            console.log(size,start, end);

            if (size < end) {
                end = size;
                start = ((size-$scope.gap) < 0) ? 0 : (size-$scope.gap);
            }else if( end%5 != 0){
                end = end - (end%5);
                start = end - $scope.gap;
            }

            for (var i = start; i < end; i++) {
                ret.push(i);
            }

            return ret;

        };

        $scope.prevPage = function () {
            if ($scope.currentPage > 0) {
                $scope.currentPage--;
            }
        };

        $scope.nextPage = function () {
            if ($scope.currentPage < $scope.pagedItems.length - 1) {
                $scope.currentPage++;
            }
        };

        $scope.setPage = function (goToPage) {
            if(goToPage != undefined){
                this.n = goToPage;
            }
            $scope.currentPage = this.n;
        };

        // functions have been describe process the data for display
        $scope.search();

    })
    .controller('aboutController', function($scope) {
        $scope.message = 'This is an example of the filtering with AngularJS';
    })
    .controller('contactController', function($scope) {
        $scope.message = 'Contact me: iggy.floyd.de@gmail.com';
    })
// new tag definition is coming here:
    .directive("customSort", function() {
        return {
            restrict: 'A', // So, we use it mostly as attribute in the parent tag
            transclude: true, // use conjtent of it
            scope: {
                order: '=', // $scope.order and $scope.sort will be bi-directional connected to our local isolated scope
                sort: '='
            },
            template :
                ' <a ng-click="sort_by(order)" style="color: #555555;">'+
                    '    <span ng-transclude></span>'+
                    '    <i ng-class="selectedCls(order)"></i>'+
                    '</a>', // template of the new control element
            link: function(scope) {
                // change sorting order
                scope.sort_by = function(newSortingOrder) {
                    var sort = scope.sort;

                    if (sort.sortingOrder == newSortingOrder){
                        sort.reverse = !sort.reverse;
                    }

                    sort.sortingOrder = newSortingOrder;
                };
                scope.selectedCls = function(column) {
                    if(column == scope.sort.sortingOrder){
                        return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
                    }
                    else{
                        return'icon-sort'
                    }
                };
            }
        }
    });