/**
 * Created by i.marfin on 1/11/16.
 */

var stop_city = function (city) {
    if (!city) return false;
    return city.toLowerCase().indexOf("\\n") !== -1;
};

/*

 "{f} {b}".printf({f: "foo", b: "bar"});

 "%s %s".printf(["foo", "bar"]);

 "%s %s".printf("foo", "bar");

 // all of which give this result:

 "foo bar"

 */

String.prototype.printf = function (obj) {
    var useArguments = false;
    var _arguments = arguments;
    var i = -1;
    if (typeof _arguments[0] == "string") {
        useArguments = true;
    }
    if (obj instanceof Array || useArguments) {
        return this.replace(/\%s/g,
            function (a, b) {
                i++;
                if (useArguments) {
                    if (typeof _arguments[i] == 'string') {
                        return _arguments[i];
                    }
                    else {
                        throw new Error("Arguments element is an invalid type");
                    }
                }
                return obj[i];
            });
    }
    else {
        return this.replace(/{([^{}]*)}/g,
            function (a, b) {
                var r = obj[b];
                return typeof r === 'string' || typeof r === 'number' ? r : a;
            });
    }
};



angular.module('mainApp', ['ngRoute','ngTable','ngSanitize'])
    .config(['$interpolateProvider', function($interpolateProvider) {
        $interpolateProvider.startSymbol('{[');
        $interpolateProvider.endSymbol(']}');
    }])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider
            // route for the home page
            .when('/', {
                templateUrl : '/static/angularjs/tutorial10/views/home.tpl',
                controller  : 'mainController'
            })
            // route for the about page
            .when('/about', {
                templateUrl : 'static/angularjs/tutorial10/views/about.tpl',
                controller  : 'aboutController'
            })

            // route for the contact page
            .when('/contact', {
                templateUrl : 'static/angularjs/tutorial10/views/contact.tpl',
                controller  : 'contactController'
            });

    }])
    // user-defiend filters
    .filter('nocmdline', function () {
        return function (value) {
            return (!value) ? '' : value.replace(/\\n/g, ' ');
        };
    })
    .filter("sanitize", ['$sce', function($sce) {
        return function(htmlCode){
            return $sce.trustAsHtml(htmlCode);
        }
    }])
    // directives
    // on-read-file: only link function is defined here
    .directive("ngTemperature",['nocmdlineFilter','ngTableParams',function(nocmdlineFilter,ngTableParams){
        return {
            restrict:'A',
            require: '^ngModel',
            scope: {
                ngModel: '='
            },
            template: '<div><h1>Weather for {[ngModel  | nocmdline ]}</h1></div>',

            controller: ['$scope', '$http','$log','$filter', function($scope, $http,$log,$filter) {
                var request='select item from weather.forecast where  woeid in (select woeid from geo.places(1) where text="%s")';
                var apiurl ='http://query.yahooapis.com/v1/public/yql?q='+'%s' +'&format=json&callback=JSON_CALLBACK';


                $scope.getTemp = function(city) {
                    if (!city) return;


                    // get temperature
                    $http({
                        method: 'JSONP',
                        url: apiurl.printf(encodeURIComponent(request.printf(city)))
                    }).then(
                        function successCallback(response) {
                            // this callback will be called asynchronously
                            // when the response is available

                            // parsing data
                            var data=response.data.query.results.channel.item;
                            var title = data.title;
                            var lat = data.lat || 'UNKNOWN';
                            var long = data.long || 'UNKNOWN';
                            var description = data.description || 'UNKNOWN';
                            //console.log(JSON.stringify(response));
                            //console.log(JSON.stringify(data));
                            //console.log(JSON.stringify(title));
                            //console.log(JSON.stringify(lat));
                            //$log.log(JSON.stringify(long));
                            //console.log(JSON.stringify(description));

                            $scope.items = [
                                {"title":title,"lat":lat,"long":long,"description":description}
                            ]

                            $scope.$parent.message2 = "<br><br><h4> The forecast has been done! </h4>"
                            $scope.$parent.usersTable = new ngTableParams({
                                page: 1,
                                count: 5
                            }, {
                                total: $scope.items.length,
                                getData: function ($defer, params) {
                                    $scope.$parent.data = params.sorting() ? $filter('orderBy')($scope.items, params.orderBy()) : $scope.items;
                                    $scope.$parent.data = params.filter() ? $filter('filter')($scope.$parent.data, params.filter()) : $scope.$parent.data;
                                    $scope.$parent.data = $scope.$parent.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
//                                    console.log($scope.data);
                                    $defer.resolve($scope.$parent.data);
                                }
                            });
                            $scope.$parent.usersTable.reload();

                        }
                    );
                }

            }],
            link: function(scope, iElement, iAttrs,ngModel) {
                // watch changing the input field
                scope.$watch(function () {
                    return ngModel.$modelValue;
                }, function(newValue) {
                    scope.$parent.message = newValue;
//                    console.log(newValue);
                    if(stop_city(newValue)) {
                        scope.getTemp(nocmdlineFilter(newValue));
                    }


                });

            }
        }
    }])

    // controllers
    .controller('mainController', function($scope, $filter, ngTableParams) {


    })
    .controller('aboutController', function($scope) {
        $scope.message = 'This is an example of the filtering with AngularJS';
    })
    .controller('contactController', function($scope) {
        $scope.message = 'Contact me: iggy.floyd.de@gmail.com';
    });

