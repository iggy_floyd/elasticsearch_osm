#!/bin/bash

echo "Installing additional dependent packages"
sudo apt-get update

sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
sudo sh -c 'echo deb "http://apt.dockerproject.org/repo debian-wheezy main" >> /etc/apt/sources.list'
sudo apt-get update
sudo apt-get install -y docker-engine
sudo service docker start
sudo docker run hello-world

